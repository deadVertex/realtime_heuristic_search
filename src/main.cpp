#include <cstdio>

// Uncomment this to disable GUI and collect results.
//#define NO_GUI

#define GLEW_STATIC
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/compatibility.hpp>

#include "grid_world.h"
#include "graph.h"
#include "simulation.h"
#include "render.h"
#include "imgui_integration.h"
#include "imgui.h"
#include "profiler.h"
#include "heap.h"

#define internal static
#define global static

enum
{
  MOUSEBUTTON_LEFT = 1,
  MOUSEBUTTON_RIGHT = 2,
};

struct View
{
  float zoomFactor, worldScale;
  glm::vec2 cameraPosition, cameraVelocity, cameraPositionOld;
  Shader shader, colourShader, gridWorldShader;
  GridWorldTilesMesh gridWorldMesh;
  GraphVisualization graph;
  bool showGraph, followRoboto;
  OpenGLStaticMesh cube, circle;
};


global Simulation* simulation;
global View view;
global bool run = true;
global int windowWidth, windowHeight;
global uint8_t mouseButtons = 0;
global double prevMouseX = 0.0, prevMouseY = 0.0;
//global uint32_t currentPathFindingRequest = 0;
//global uint32_t iteration = 1;
//global uint32_t cellsTravelled = 0;
//global bool pauseSimulation = true;


inline uint32_t ToGridSpace( const GridWorld &world, float x, float y )
{
  int ix = glm::floor( x );
  int iy = glm::floor( y );
  ix = glm::max( ix, 0 );
  iy = glm::max( iy, 0 );
  uint32_t node = ix + world.m_width * iy;
  return node;
}

inline glm::vec2 ToWorldSpace( const GridWorld &world, uint32_t cellIdx )
{
  uint32_t x = cellIdx % world.m_width;
  uint32_t y = cellIdx / world.m_width;
  glm::vec2 result{ x + 0.5f, y + 0.5f };
  return result;
}


internal void LoadContent()
{
  const char* vertexSource =
    "#version 330\n"
    "layout(location = 0) in vec3 position;\n"
    "uniform mat4 in_mCombined;\n"
    "void main()\n"
    "{\n"
    "	gl_Position = in_mCombined * vec4( position, 1.0);\n"
    "}";

  const char* fragmentSource = "#version 330\n"
                               "uniform vec4 colour;\n"
                               "out vec4 out_vColour;\n"
                               "void main()\n"
                               "{\n"
                               "out_vColour = colour;\n"
                               "}";

  const char* colourVertexSource =
    "#version 330\n"
    "layout(location = 0) in vec3 position;\n"
    "layout(location = 4) in vec3 vertexColour;\n"
    "uniform mat4 in_mCombined;\n"
    "out vec4 colour;\n"
    "void main()\n"
    "{\n"
    "	 gl_Position = in_mCombined * vec4( position, 1.0);\n"
    "  colour = vec4( vertexColour, 1.0 );\n"
    "}";

  const char* colourFragmentSource =
    "#version 330\n"
    "in vec4 colour;\n"
    "out vec4 out_vColour;\n"
    "void main()\n"
    "{\n"
    "  out_vColour = vec4( 1.0, 0.0, 0.0, 1.0 );\n"
    "}";

  const char *gridVertexSource =
    "#version 330\n"
    "layout(location = 0) in vec3 position;\n"
    "layout(location = 8 ) in vec2 offset;\n"
    "layout(location = 9 ) in float state;\n"
    "uniform mat4 in_mCombined;\n"
    "out vec4 colour;\n"
    "void main()\n"
    "{\n"
    "  colour = vec4( state, state, state, 1.0 );\n"
    "	 gl_Position = in_mCombined * vec4( position + vec3( offset, 0 ), 1.0);\n"
    "}";

  const char *gridFragmentSource =
    "#version 330\n"
    "in vec4 colour;\n"
    "out vec4 out_vColour;\n"
    "void main()\n"
    "{\n"
    "  out_vColour = colour;\n"
    "}";

  view.shader = CreateShader( vertexSource, fragmentSource );
  view.colourShader = CreateShader( colourVertexSource, colourFragmentSource );
  view.gridWorldShader = CreateShader( gridVertexSource, gridFragmentSource );
  glClearColor( 0.2f, 0.2f, 0.2f, 1.0f );
  glEnable( GL_DEPTH_TEST );
  glEnable( GL_BLEND );
  glDepthFunc( GL_LESS );

  view.worldScale =
    glm::max( simulation->world.m_width, simulation->world.m_height );

  view.gridWorldMesh = BuildGridWorldTilesMesh( simulation->world );
  view.graph =
    CreateGraphVisualization( simulation->graph, simulation->nodeGrid );

  view.cube = CreateCubeMesh();
  view.circle = CreateCircleMesh( 0.4f, 12, true );
}

internal void OnRenderFrame( GLFWwindow *window, float interp )
{
  glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

  glUseProgram( view.shader.program );
  glm::vec3 centrePoint;
  glm::vec2 localCameraPosition =
    glm::lerp( view.cameraPositionOld, view.cameraPosition, interp );
  centrePoint.x = -localCameraPosition.x;
  centrePoint.y = -localCameraPosition.y;

  float aspectRatio = (float)windowWidth / (float)windowHeight;
  float scale = view.zoomFactor;
  float w = 0.5f;
  float h = 0.5f / aspectRatio;
  glm::mat4 projectionMatrix = glm::ortho( -w, w, h, -h, -1.0f, 1.0f );
  glm::mat4 viewMatrix;
  float x = scale / simulation->world.m_width;
  float y = scale / simulation->world.m_height;
  viewMatrix = glm::scale( viewMatrix, glm::vec3{ x, y, 1.0f } );


  auto roboto = simulation->roboto;
  glm::vec3 p = glm::lerp( roboto.oldWorldP, roboto.newWorldP, interp );
  float angle = glm::lerp( roboto.oldAngle, roboto.newAngle, interp );

  if ( view.followRoboto )
  {
    viewMatrix = glm::translate( viewMatrix, -p );
  }
  else
  {
    viewMatrix = glm::translate( viewMatrix, centrePoint );
  }

  glm::mat4 combined = projectionMatrix * viewMatrix;
  glUseProgram( view.gridWorldShader.program );
  RenderGridWorld( combined, scale, view.gridWorldShader, view.gridWorldMesh );
  glUseProgram( view.shader.program );

  if ( view.showGraph )
  {
    DrawGraph( combined, view.shader, view.graph,
        glm::vec4{0.5f, 0.9f, 0.0f, 1.0f} );
  }

  glm::mat4 modelMatrix = glm::translate( glm::mat4(), p );
  modelMatrix = glm::rotate( modelMatrix, angle, glm::vec3{ 0, 0, 1 } );
  modelMatrix = glm::scale( modelMatrix, glm::vec3{ 0.8f } );
  combined = projectionMatrix * viewMatrix * modelMatrix;

  glUniform4f( view.shader.colourLocation, 0.2f, 0.5f, 1.0f, 1.0f );
  glUniformMatrix4fv( view.shader.matrixLocation, 1, GL_FALSE,
                      glm::value_ptr( combined ) );
  OpenGLDrawStaticMesh( view.cube );

  auto goalP = ToWorldSpace( simulation->world, roboto.goalCell );
  modelMatrix = glm::translate( glm::mat4(), glm::vec3{ goalP, 0.5f } );
  glUniform4f( view.shader.colourLocation, 0.1f, 0.3f, 1.0f, 1.0f );
  combined = projectionMatrix * viewMatrix * modelMatrix;
  glUniformMatrix4fv( view.shader.matrixLocation, 1, GL_FALSE,
                      glm::value_ptr( combined ) );
  OpenGLDrawStaticMesh( view.circle );

  auto request =
    simulation->pathFindingRequests[roboto.currentPathFindingRequest];
  auto positionResult =
    simulation->nodeGrid.GetNodePosition( request.startNode );
  glm::vec3 startP{positionResult.position.x + 0.5f,
                   positionResult.position.y + 0.5f, 0.5f};
  modelMatrix = glm::translate( glm::mat4(), startP );
  glUniform4f( view.shader.colourLocation, 0.3f, 0.7f, 0.1f, 1.0f );
  combined = projectionMatrix * viewMatrix * modelMatrix;
  glUniformMatrix4fv( view.shader.matrixLocation, 1, GL_FALSE,
                      glm::value_ptr( combined ) );
  OpenGLDrawStaticMesh( view.circle );

  double mousePosX, mousePosY;
  glfwGetCursorPos( window, &mousePosX, &mousePosY );
  uiNewFrame( (float)mousePosX, (float)mousePosY, 1.0f / 60.0f );

  ImGui::Checkbox( "Draw Graph", &view.showGraph );
  ImGui::Checkbox( "Follow Robot", &view.followRoboto );
  ImGui::LabelText( "Target Cell", "%d", simulation->roboto.targetCell );
  ImGui::LabelText( "Current Cell", "%d", simulation->roboto.currentCell );
  ImGui::LabelText( "Goal Cell", "%d", simulation->roboto.goalCell );
  ImGui::LabelText( "Iteration", "%d", roboto.iteration );
  ImGui::LabelText( "Current Path Length", "%d", roboto.cellsTravelled );
  ImGui::LabelText( "Current Pathfinding Request", "%d",
                    roboto.currentPathFindingRequest );
  ImGui::LabelText( "Optimal Path Length", "%d", request.pathLength );
  if ( simulation->pauseSimulation )
  {
    if ( ImGui::Button( "Run Simulation" ) )
    {
      simulation->pauseSimulation = false;
    }
  }
  else
  {
    if ( ImGui::Button( "Pause Simulation" ) )
    {
      simulation->pauseSimulation = true;
    }
  }
  // ... UI CODE GOES HERE

  ImGui::Render();
}

internal void WindowResizeCallback( GLFWwindow *window, int width, int height )
{
  windowWidth = width;
  windowHeight = height;
  glViewport( 0, 0, width, height );
}

internal void MouseScrollCallback( GLFWwindow *window, double xoffset,
                                 double yoffset )
{
  view.zoomFactor += (float)yoffset;
  if ( view.zoomFactor < 0.01f )
  {
    view.zoomFactor = 0.01f;
  }
}

internal void MouseButtonCallback( GLFWwindow *window, int button, int action,
                                   int mods )
{
  if ( action == GLFW_PRESS )
  {
    if ( button == GLFW_MOUSE_BUTTON_LEFT )
    {
      mouseButtons |= MOUSEBUTTON_LEFT;
    }
    else if ( button == GLFW_MOUSE_BUTTON_RIGHT )
    {
      mouseButtons |= MOUSEBUTTON_RIGHT;
    }
  }
  else if ( action == GLFW_RELEASE )
  {
    if ( button == GLFW_MOUSE_BUTTON_LEFT )
    {
      mouseButtons &= ~MOUSEBUTTON_LEFT;
    }
    else if ( button == GLFW_MOUSE_BUTTON_RIGHT )
    {
      mouseButtons &= ~MOUSEBUTTON_RIGHT;
    }
  }
  uiOnMouseButtonInput( window, button, action, mods );
}

internal void CharacterCallback( GLFWwindow *window, unsigned int c )
{
  uiOnCharInput( window, c );
}

internal void KeyCallback( GLFWwindow *window, int key, int scancode,
                           int action, int mods )
{
  if ( action == GLFW_PRESS )
  {
    switch ( key )
    {
    case GLFW_KEY_ESCAPE:
      run = false;
      break;
    default:
      break;
    }
  }
  else if ( action == GLFW_RELEASE )
  {
  }

  uiOnKeyInput( window, key, scancode, action, mods );
}

#ifdef _MSC_VER
internal void APIENTRY
  OpenGLReportErrorMessage( GLenum source, GLenum type, GLuint id,
                            GLenum severity, GLsizei length,
                            const GLchar *message, const void *userParam )
#else
internal void APIENTRY
  OpenGLReportErrorMessage( GLenum source, GLenum type, GLuint id,
                            GLenum severity, GLsizei length,
                            const GLchar *message, void *userParam )
#endif
{
  const char *typeStr = nullptr;
  switch ( type )
  {
    case GL_DEBUG_TYPE_ERROR:
      typeStr = "ERROR";
      break;
    case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
      typeStr = "DEPRECATED_BEHAVIOR";
      break;
    case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
      typeStr = "UNDEFINED_BEHAVIOR";
      break;
    case GL_DEBUG_TYPE_PORTABILITY:
      typeStr = "PORTABILITY";
      break;
    case GL_DEBUG_TYPE_PERFORMANCE:
      typeStr = "PERFORMANCE";
      break;
    case GL_DEBUG_TYPE_OTHER:
      typeStr = "OTHER";
      return;
      break;
    default:
      typeStr = "";
      break;
  }

  const char *severityStr = nullptr;
  switch ( severity )
  {
    case GL_DEBUG_SEVERITY_LOW:
      severityStr = "LOW";
      break;
    case GL_DEBUG_SEVERITY_MEDIUM:
      severityStr = "MEDIUM";
      break;
    case GL_DEBUG_SEVERITY_HIGH:
      severityStr = "HIGH";
      break;
    default:
      severityStr = "";
      break;
  }

  fprintf( stdout, "OPENGL|%s:%s:%s\n", typeStr, severityStr, message );
}

internal GLFWwindow* CreateWindow()
{
  GLFWwindow *window = nullptr;

  if ( glfwInit() )
  {
    glfwWindowHint( GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE );
    glfwWindowHint( GLFW_DECORATED, true );

    window = glfwCreateWindow(
      1280, 720, "Real-Time Heuristic Search with a Priority Queue", NULL,
      NULL );
    windowWidth = 1280;
    windowHeight = 720;

    if ( window )
    {
      glfwMakeContextCurrent( window );
      glfwSetWindowSizeCallback( window, WindowResizeCallback );
      glfwSetKeyCallback( window, KeyCallback );
      glfwSetScrollCallback( window, MouseScrollCallback );
      glfwSetMouseButtonCallback( window, MouseButtonCallback );
      glfwSetCharCallback( window, CharacterCallback );
      glfwSwapInterval( 0 );

      GLenum err = glewInit();
      if ( err == GLEW_OK )
      {
        fprintf( stdout, "Using GLEW %s.\n", glewGetString( GLEW_VERSION ) );

        glEnable( GL_DEBUG_OUTPUT_SYNCHRONOUS );
        glDebugMessageCallback( OpenGLReportErrorMessage, nullptr );
        GLuint unusedIds = 0;
        glDebugMessageControl( GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0,
                               &unusedIds, GL_TRUE );
      }
      else
      {
        glfwDestroyWindow( window );
        glfwTerminate();
        fprintf( stderr, "GLEW ERROR: %s\n", glewGetErrorString( err ) );
        window = nullptr;
      }
    }
    else
    {
      glfwTerminate();
      fprintf( stderr, "Failed to create GLFW window.\n" );
    }
  }
  else
  {
    fprintf( stderr, "Failed to initialize GLFW.\n" );
  }
  return window;
}

struct QueueEntry
{
  uint32_t cellIdx;
  float delta;
};

inline bool operator==(const QueueEntry &a, const QueueEntry &b)
{
  return a.cellIdx == b.cellIdx;
}

struct CompareQueueEntries
{
  bool operator() ( const QueueEntry &a, const QueueEntry &b ) const
  {
    return a.delta > b.delta;
  }
};

global Heap< QueueEntry, CompareQueueEntries > queue( 30 );
global float *heuristicArray;
internal void AddNeighboursToQueue( GridWorld *world, uint32_t cellIdx,
                                    float delta )
{
  int cx = cellIdx % world->m_width;
  int cy = cellIdx / world->m_width;

  int minX = glm::max( cx - 1, 0 );
  int minY = glm::max( cy - 1, 0 );

  int maxX = glm::min( cx + 1, (int)world->m_width );
  int maxY = glm::min( cy + 1, (int)world->m_height );

  for ( int y = minY; y <= maxY; ++y )
  {
    for ( int x = minX; x <= maxX; ++x )
    {
      if ( x != cx || y != cy )
      {
        QueueEntry entry = {};
        entry.cellIdx = x + y * world->m_width;
        if ( !( world->m_cells[entry.cellIdx] & CELL_STATE_BLOCKED ) )
        {
          entry.delta = delta;
          if ( !queue.Contains( entry ) )
          {
            if ( queue.GetSize() == queue.GetCapacity() )
            {
              queue.ReplaceMin( entry );
            }
            else
            {
              queue.Push( entry );
            }
          }
        }
      }
    }
  }
}

internal void StateUpdate( GridWorld *world, uint32_t cellIdx )
{
  int cx = cellIdx % world->m_width;
  int cy = cellIdx / world->m_width;

  int minX = glm::max( cx - 1, 0 );
  int minY = glm::max( cy - 1, 0 );

  int maxX = glm::min( cx + 1, (int)world->m_width );
  int maxY = glm::min( cy + 1, (int)world->m_height );

  bool found = false;
  float minF = 0.0f;
  for ( int y = minY; y <= maxY; ++y )
  {
    for ( int x = minX; x <= maxX; ++x )
    {
      if ( x != cx || y != cy )
      {
        uint32_t idx = x + y * world->m_width;
        if ( !( world->m_cells[idx] & CELL_STATE_BLOCKED ) )
        {
          float dx = x - cx;
          float dy = y - cy;
          float cost = glm::sqrt( dx * dx + dy * dy );
          float heuristic = heuristicArray[idx];
          float f = cost + heuristic;
          if ( !found || f < minF )
          {
            minF = f;
            found = true;
          }
        }
      }
    }
  }

  if ( found )
  {
    float heuristic = heuristicArray[cellIdx];
    float delta = minF - heuristic;

    if ( delta > 0.0f )
    {
      heuristicArray[cellIdx] = minF;
      AddNeighboursToQueue( world, cellIdx, delta );
    }
  }
}

internal void UpdateVisibility( GridWorld *world, uint32_t cellIdx,
                                int radius )
{
  int cx = cellIdx % world->m_width;
  int cy = cellIdx / world->m_width;

  int minX = glm::max( cx - radius, 0 );
  int minY = glm::max( cy - radius, 0 );

  int maxX = glm::min( cx + radius, (int)world->m_width );
  int maxY = glm::min( cy + radius, (int)world->m_height );

  for ( int y = minY; y <= maxY; ++y )
  {
    for ( int x = minX; x <= maxX; ++x )
    {
      int dx = x - cx;
      int dy = y - cy;
      int r = sqrtl( dx * dx + dy + dy );
      if ( r <= radius )
      {
        uint32_t idx = x + y * world->m_width;
        world->m_cells[idx] |= CELL_STATE_EXPLORED;
      }
    }
  }
}

internal void SetupRoboto( Roboto *roboto )
{
  auto request =
    simulation->pathFindingRequests[roboto->currentPathFindingRequest];
  auto startResult = simulation->nodeGrid.GetNodePosition( request.startNode );
  assert( startResult.isValid );
  auto goalResult = simulation->nodeGrid.GetNodePosition( request.goalNode );
  assert( goalResult.isValid );
  auto startP = startResult.position;
  auto goalP = goalResult.position;

  auto robotoP = glm::vec3{ startP.x + 0.5f, startP.y + 0.5f, 0.5f };
  roboto->newWorldP = robotoP;
  roboto->currentCell =
    ToGridSpace( simulation->world, robotoP.x, robotoP.y );
  roboto->targetCell = roboto->currentCell;
  roboto->goalCell = goalP.x + goalP.y * simulation->world.m_width;;
  assert( !( simulation->world.m_cells[roboto->goalCell] &
             CELL_STATE_BLOCKED ) );
  roboto->cellsTravelled = 0;

  queue.Clear();
}

void ResetHeuristicTable()
{
  uint32_t count = simulation->world.m_width * simulation->world.m_height;
  auto gp = ToWorldSpace( simulation->world, simulation->roboto.goalCell );
  for ( uint32_t i = 0; i < count; ++i )
  {
    auto p = ToWorldSpace( simulation->world, i );
    float dist = glm::length( gp - p );
    heuristicArray[i] = dist;
  }
}

internal void UpdateRoboto( GridWorld *world, Roboto *roboto )
{
  if ( roboto->targetCell == roboto->currentCell )
  {
    if ( roboto->currentCell != roboto->goalCell )
    {
      StateUpdate( world, roboto->currentCell );

      int N = 30;
      int i = 0;
      while ( i < N && !queue.IsEmpty() )
      {
        auto el = queue.Front();
        queue.Pop();
        if ( el.cellIdx != roboto->goalCell )
        {
          StateUpdate( world, el.cellIdx );
          i++;
        }
      }
      auto cellIdx = roboto->currentCell;
      int cx = cellIdx % world->m_width;
      int cy = cellIdx / world->m_width;

      int minX = glm::max( cx - 1, 0 );
      int minY = glm::max( cy - 1, 0 );

      int maxX = glm::min( cx + 1, (int)world->m_width );
      int maxY = glm::min( cy + 1, (int)world->m_height );

      bool found = false;
      float minF = 0.0f;
      uint32_t minIdx = 0;
      for ( int y = minY; y <= maxY; ++y )
      {
        for ( int x = minX; x <= maxX; ++x )
        {
          if ( x != cx || y != cy )
          {
            uint32_t cellIdx = x + y * world->m_width;
            if ( !( world->m_cells[cellIdx] & CELL_STATE_BLOCKED ) &&
                 ( cellIdx != roboto->lastCell ) &&
                 ( cellIdx != roboto->currentCell ) )
            {
              float dx = x - cx;
              float dy = y - cy;
              float cost = glm::sqrt( dx * dx + dy * dy );
              float heuristic = heuristicArray[cellIdx];
              float f = cost + heuristic;
              if ( !found || f < minF )
              {
                minF = f;
                minIdx = x + y * world->m_width;
                found = true;
              }
            }
          }
        }
      }

      if ( found )
      {
        roboto->lastCell = roboto->targetCell;
        roboto->targetCell = minIdx;
        roboto->cellsTravelled++;
      }
    }
    else
    {
      if ( roboto->currentPathFindingRequest <
           simulation->numPathFindingRequests )
      {
        roboto->cellsTravelled++; // Need to include travelling to the goal cell.
        printf( "%d, %d\n", roboto->iteration, roboto->cellsTravelled );
        SetupRoboto( roboto );
        roboto->iteration++;
        if ( roboto->iteration == simulation->iterationsPerRequest )
        {
          roboto->currentPathFindingRequest++;
          if ( roboto->currentPathFindingRequest >=
               simulation->numPathFindingRequests )
          {
            run = false;
          }
          else
          {
            printf(
              "Optimal Path Length: %d\n",
              simulation->pathFindingRequests[roboto->currentPathFindingRequest]
                .pathLength );
            roboto->iteration = 0;
            SetupRoboto( roboto );
            ResetHeuristicTable();
          }
        }
      }
    }
  }
}

internal void OnUpdateTick( GLFWwindow *window, float dt )
{
#ifndef NO_GUI
  view.cameraVelocity = glm::vec2{0};
  double mousePosX, mousePosY;
  glfwGetCursorPos( window, &mousePosX, &mousePosY );
  if ( mouseButtons & MOUSEBUTTON_RIGHT )
  {
    view.cameraVelocity.x = (float)( prevMouseX - mousePosX );
    view.cameraVelocity.y = (float)( prevMouseY - mousePosY );
    view.cameraVelocity *= 10.0f / view.zoomFactor;
  }
  prevMouseX = mousePosX;
  prevMouseY = mousePosY;

  view.cameraPositionOld = view.cameraPosition;
  view.cameraPosition += view.cameraVelocity * dt;
#endif


  auto roboto = &simulation->roboto;
  roboto->oldWorldP = roboto->newWorldP;
  roboto->oldAngle = roboto->newAngle;
  if ( !simulation->pauseSimulation )
  {
    UpdateRoboto( &simulation->world, roboto );

    auto targetPosition = ToWorldSpace( simulation->world, roboto->targetCell );
    glm::vec2 velocity = targetPosition - glm::vec2{roboto->newWorldP};
    auto distance = glm::length( velocity );
    if ( distance > 0.0f )
    {
      auto direction = glm::normalize( velocity );

      float speed = 500.0f;
      float dp = speed * dt;
      dp = glm::min( dp, distance );

      roboto->newAngle = glm::atan( -direction.x, direction.y );

      roboto->newWorldP.x += direction.x * dp;
      roboto->newWorldP.y += direction.y * dp;
      roboto->currentCell = ToGridSpace( simulation->world, roboto->newWorldP.x,
                                         roboto->newWorldP.y );
      UpdateVisibility( &simulation->world, roboto->currentCell, 5 );
    }
  }

#ifndef NO_GUI
  UpdateGridWorldTilesMesh( simulation->world, &view.gridWorldMesh );
#endif
}

int main( int argc, char **argv )
{
  view.worldScale = 1.0f;
  view.zoomFactor = 1.0f;
  const char* config = "../content/maps/drywatergulch.map";
  simulation = new Simulation;
  simulation->numPathFindingRequests = 5;
#ifndef NO_GUI
  simulation->pauseSimulation = true;
#else
  simulation->pauseSimulation = false;
#endif
  simulation->iterationsPerRequest = 5;
  if ( argc >= 2 )
  {
    config = argv[1];
  }
  if ( argc >= 3 )
  {
    int numRequests = atoi( argv[2] );
    if ( numRequests > 0 && numRequests < (int)MAX_PATH_FINDING_REQUESTS )
    {
      simulation->numPathFindingRequests = numRequests;
    }
    else
    {
      printf( "Invalid number of requests.\n" );
      return -1;
    }
  }
  if ( argc >= 4 )
  {
    int iterationsPerRequest = atoi( argv[3] );
    if ( iterationsPerRequest > 0 )
    {
      simulation->iterationsPerRequest = iterationsPerRequest;
    }
    else
    {
      printf( "Invalid value for number of iterations per request.\n" );
      return -1;
    }
  }

#ifndef NO_GUI
  auto window = CreateWindow();
  if ( !window )
  {
    return -1;
  }
#endif

  bool success = InitializeSimulation( simulation, config );
  assert( success );

  auto roboto = &simulation->roboto;
  roboto->iteration = 0;
  roboto->currentPathFindingRequest = 1;
  SetupRoboto( &simulation->roboto );
  printf( "Optimal Path Length: %d\n",
          simulation->pathFindingRequests[roboto->currentPathFindingRequest]
            .pathLength );

  uint32_t count = simulation->world.m_width * simulation->world.m_height;
  heuristicArray = new float[count];
  ResetHeuristicTable();

#ifndef NO_GUI
  LoadContent();
  uiInit( window );

  double t = 0.0;
  float logicHz = 30.0f;
  float logicTimestep = 1.0f / logicHz;
  double maxFrameTime = 0.25; // Used to prevent the simulation from being
                              // affected by break points.

  double currentTime = glfwGetTime();
  double accumulator = 0.0;

  while ( run )
  {
    double newTime = glfwGetTime();
    double frameTime = newTime - currentTime;
    if ( frameTime > maxFrameTime )
      frameTime = maxFrameTime;
    currentTime = newTime;

    accumulator += frameTime;

    while ( accumulator >= logicTimestep )
    {
      glfwPollEvents();
      if ( glfwWindowShouldClose( window ) )
      {
        run = false;
      }
      // Logic is updated at 30Hz
      OnUpdateTick( window, logicTimestep );

      t += logicTimestep;
      accumulator -= logicTimestep;
    }

    double interp = accumulator / logicTimestep;
    // Rendering is done at 60Hz or faster if vsync is disabled.
    OnRenderFrame( window, (float)interp );
    glfwSwapBuffers( window );
  }
  uiShutdown();
  glfwTerminate();
#else
  if ( glfwInit() )
  {
    double currentTime = glfwGetTime();
    while ( run )
    {
      double newTime = glfwGetTime();
      double frameTime = newTime - currentTime;
      OnUpdateTick( nullptr, frameTime );
    }
  }
  else
  {
    fprintf( stderr, "Failed to initialize GLFW.\n" );
  }
#endif
  delete[] heuristicArray;
  delete simulation;
  return 0;
}
