#include "simulation.h"

#include <queue>
#include <cstdio>
#include <cstdarg>
#include <cstdint>
#include <queue>
#include <random>
#include <chrono>

#include "glm/glm.hpp"
#include "glm/gtc/type_ptr.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtx/compatibility.hpp"

#include "grid_world.h"
#include "graph.h"
#include "profiler.h"

#define MAX_PATH_LENGTH 2048

static void CreatePathFindingRequests( Simulation *simulation )
{
  uint32_t pathNodes[MAX_PATH_LENGTH];
  auto seed = std::chrono::system_clock::now().time_since_epoch().count();
  std::default_random_engine generator( seed );
  std::uniform_int_distribution<int> distribution(
    0, simulation->nodeGrid.GetNumNodes() );

  for ( uint32_t i = 0; i < simulation->numPathFindingRequests; ++i )
  {
    auto request = simulation->pathFindingRequests + i;
    request->startNode = distribution( generator );
    request->goalNode = distribution( generator );

    // Make sure that a path exists.
    int pathLength = AStar( simulation->graph, simulation->nodeGrid, pathNodes,
                            MAX_PATH_LENGTH, request->startNode,
                            request->goalNode, simulation->aStarContext );
    while ( pathLength <= 0 )
    {
      request->startNode = distribution( generator );
      request->goalNode = distribution( generator );
      pathLength = AStar( simulation->graph, simulation->nodeGrid, pathNodes,
                          MAX_PATH_LENGTH, request->startNode,
                          request->goalNode, simulation->aStarContext );
    }
    request->pathLength = pathLength;
  }
}

void GenerateGraphFromGridWorld( Graph *graph, NodeGrid *grid,
                                 const GridWorld &world )
{
  for ( uint32_t y = 0; y < world.m_height - 1; ++y )
  {
    for ( uint32_t x = 0; x < world.m_width - 1; ++x )
    {
      if ( !world.m_cells[x + ( y * world.m_width )] & CELL_STATE_BLOCKED )
      {
        uint32_t to;
        NodePosition p = { x, y };
        uint32_t from = grid->AddNode( p );
        if ( x > 0 )
        {
          NodePosition p2{ x - 1, y };
          if ( grid->GetNodeAtPosition( p2, &to ) )
          {
            graph->AddConnection( to, from, 1.0f );
            graph->AddConnection( from, to, 1.0f );
          }
        }
        if ( y > 0 )
        {
          NodePosition p2{ x, y - 1 };
          if ( grid->GetNodeAtPosition( p2, &to ) )
          {
            graph->AddConnection( to, from, 1.0f );
            graph->AddConnection( from, to, 1.0f );
          }
        }
        if ( x > 0 && y > 0 )
        {
          NodePosition p2{ x - 1, y - 1 };
          if ( grid->GetNodeAtPosition( p2, &to ) )
          {
            float d = CalculateEuclideanDistance( p, p2 );
            graph->AddConnection( to, from, d );
            graph->AddConnection( from, to, d );
          }
        }
        if ( x < world.m_width - 1 && y > 0 )
        {
          NodePosition p2{ x + 1, y - 1 };
          if ( grid->GetNodeAtPosition( p2, &to ) )
          {
            float d = CalculateEuclideanDistance( p, p2 );
            graph->AddConnection( to, from, d );
            graph->AddConnection( from, to, d );
          }
        }
      }
    }
  }
}

bool InitializeSimulation( Simulation *simulation, const char *config )
{
  auto f = fopen( config, "rb" );
  if ( !f )
  {
    printf( "Could not find file %s.", config );
    return false;
  }
  fseek( f, 0, SEEK_END );
  auto size = ftell( f );
  fseek( f, 0, SEEK_SET );
  assert( size );
  char *mapData = new char[size];
  fread( mapData, 1, size, f );
  fclose( f );

  Deserialize( &simulation->world, mapData, size );
  delete[] mapData;
  GenerateGraphFromGridWorld( &simulation->graph, &simulation->nodeGrid,
                              simulation->world );

  simulation->aStarContext = CreateAStarContext();

  CreatePathFindingRequests( simulation );

  return true;
}
