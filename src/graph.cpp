#include "graph.h"

#include <algorithm>
#include <cassert>

Graph::Graph( uint32_t capacity ) : m_numConnections( 0 ), m_hashMap( capacity )
{
}

bool Graph::AddConnection( uint32_t to, uint32_t from, float cost )
{
  assert( to != from );
  Connection connection;
  connection.m_to = to;
  connection.m_from = from;
  connection.m_cost = cost;
  auto existingConnectionArray = m_hashMap.retrieve( from );
  if ( existingConnectionArray )
  {
    for ( uint32_t i = 0; i < existingConnectionArray->numConnections; ++i )
    {
      if ( existingConnectionArray->connections[i].m_to == to )
      {
        // Ignore the duplicate.
        existingConnectionArray->connections[i] = connection;
        return false;
      }
    }
    assert( existingConnectionArray->numConnections <
            MAX_CONNECTIONS_PER_NODE );
    existingConnectionArray
      ->connections[existingConnectionArray->numConnections++] = connection;
    m_numConnections++;
    return true;
  }
  ConnectionArray newConnectionArray;
  newConnectionArray.connections[0] = connection;
  newConnectionArray.numConnections = 1;
  m_hashMap.add( from, newConnectionArray );
  m_numConnections++;
  return true;
}

void Graph::RemoveConnection( uint32_t to, uint32_t from )
{
  auto connectionArray = m_hashMap.retrieve( from );
  if ( connectionArray )
  {
    for ( uint32_t i = 0; i < connectionArray->numConnections; ++i )
    {
      if ( connectionArray->connections[i].m_to == to )
      {
        connectionArray->connections[i] =
          connectionArray->connections[connectionArray->numConnections - 1];
        connectionArray->numConnections--;
        m_numConnections--;
        break;
      }
    }
    if ( connectionArray->numConnections == 0 )
    {
      m_hashMap.remove( from );
    }
  }
}

uint32_t Graph::GetConnections( Connection *connections, uint32_t size,
                                uint32_t fromNode ) const
{
  auto connectionArray = m_hashMap.retrieve( fromNode );
  if ( connectionArray )
  {
    auto n = std::min( size, connectionArray->numConnections );
    std::copy_n( connectionArray->connections, n, connections );
    return n;
  }
  return 0;
}

uint32_t Graph::GetDegree( uint32_t node ) const
{
  Connection connections[MAX_CONNECTIONS_PER_NODE];
  return GetConnections( connections, MAX_CONNECTIONS_PER_NODE, node );
}

uint32_t Graph::GetNumConnections() const { return m_numConnections; }

uint32_t Graph::GetAllConnections( Connection *connections,
                                   uint32_t size ) const
{
  uint32_t numConnections = 0;
  for ( uint32_t index = 0; index < MAX_NODES;
        index = m_hashMap.get_next_index( index ) )
  {
    auto connectionArray = &m_hashMap.m_values[index];
    for ( uint32_t i = 0; i < connectionArray->numConnections; ++i )
    {
      if ( numConnections >= size )
      {
        return size;
      }
      connections[numConnections++] = connectionArray->connections[i];
    }
  }
  return numConnections;
}

bool Graph::IsConnected( uint32_t node1, uint32_t node2 ) const
{
  Connection connections[8];
  uint32_t n = GetConnections( connections, 8, node1 );
  for ( uint32_t i = 0; i < n; ++i )
  {
    if ( connections[i].m_to == node2 )
    {
      return true;
    }
  }
  return false;
}

NodeGrid::NodeGrid( uint32_t capacity )
  : m_xMin( 0xFFFFFFFF ), m_yMin( 0xFFFFFFFF ), m_xMax( 0 ), m_yMax( 0 ),
    m_hashMap( capacity )
{
  m_nodePositions.reserve( capacity );
}

uint32_t NodeGrid::AddNode( NodePosition p )
{
  m_nodePositions.push_back( p );
  m_xMin = std::min( p.x, m_xMin );
  m_xMax = std::max( p.x, m_xMax );
  m_yMin = std::min( p.y, m_yMin );
  m_yMax = std::max( p.y, m_yMax );
  uint32_t value = (uint32_t)m_nodePositions.size() - 1;
  m_hashMap.add( p, value );
  return m_nodePositions.size() - 1;
}

void NodeGrid::RemoveNode( uint32_t node )
{
  if ( node < m_nodePositions.size() )
  {
    auto position = m_nodePositions[node];
    m_hashMap.remove( position );
    // TODO: Figure out how what to do with nodePositions array.
  }
}

void NodeGrid::SetNodePosition( uint32_t node, NodePosition p )
{
  // TODO: Check that we don't need to update xMin, xMax, etc.
  assert( node < m_nodePositions.size() );
  auto &currentPosition = m_nodePositions[ node ];
  m_hashMap.remove( currentPosition );
  m_hashMap.add( p, node );
  currentPosition = p;
}

GetNodePositionResult NodeGrid::GetNodePosition( uint32_t node ) const
{
  GetNodePositionResult result = {};
  if ( node < m_nodePositions.size() )
  {
    result.position = m_nodePositions[node];
    result.isValid = true;
  }
  return result;
}

bool NodeGrid::GetNodeAtPosition( NodePosition p, uint32_t *node ) const
{
  auto value = m_hashMap.retrieve( p );
  if ( value )
  {
    *node = *value;
    return true;
  }
  return false;
}

bool NodeGrid::GetNodeAtPosition( uint32_t x, uint32_t y, uint32_t *node ) const
{
  NodePosition p = { x, y };
  return GetNodeAtPosition( p, node );
}
