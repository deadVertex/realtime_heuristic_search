/*
 * Most of this code was copied from the ImGUI opengl3 demo and modified to
 * work with my code for input handling and rendering.
 */

// ImGui GLFW binding with OpenGL3 + shaders
// https://github.com/ocornut/imgui

#include "imgui.h"
#include "imgui_integration.h"

#define GLEW_STATIC
#define GLEW_NO_GLU
#include <GL/glew.h>
#include <GLFW/glfw3.h>

static GLFWwindow *g_Window = NULL;
static double g_Time = 0.0f;
static bool g_MousePressed[3] = {false, false, false};
static float g_MouseWheel = 0.0f;
static GLuint g_FontTexture = 0;
static int g_ShaderHandle = 0, g_VertHandle = 0, g_FragHandle = 0;
static int g_AttribLocationTex = 0, g_AttribLocationProjMtx = 0;
static int g_AttribLocationPosition = 0, g_AttribLocationUV = 0,
           g_AttribLocationColor = 0;
static size_t g_VboSize = 0;
static unsigned int g_VboHandle = 0, g_VaoHandle = 0;

static void uiOnRender(ImDrawList** const cmd_lists, int cmd_lists_count)
{
  if ( cmd_lists_count == 0 )
    return;

  glEnable( GL_BLEND );
  glBlendEquation( GL_FUNC_ADD );
  glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
  glDisable( GL_CULL_FACE );
  glDisable( GL_DEPTH_TEST );
  glEnable( GL_SCISSOR_TEST );
  glActiveTexture( GL_TEXTURE0 );

  const float width = ImGui::GetIO().DisplaySize.x;
  const float height = ImGui::GetIO().DisplaySize.y;
  const float ortho_projection[4][4] = {
    {2.0f / width, 0.0f, 0.0f, 0.0f},
    {0.0f, 2.0f / -height, 0.0f, 0.0f},
    {0.0f, 0.0f, -1.0f, 0.0f},
    {-1.0f, 1.0f, 0.0f, 1.0f},
  };
  glUseProgram( g_ShaderHandle );
  glUniform1i( g_AttribLocationTex, 0 );
  glUniformMatrix4fv( g_AttribLocationProjMtx, 1, GL_FALSE,
                      &ortho_projection[0][0] );

  size_t total_vtx_count = 0;
  for ( int n = 0; n < cmd_lists_count; n++ )
    total_vtx_count += cmd_lists[n]->vtx_buffer.size();
  glBindBuffer( GL_ARRAY_BUFFER, g_VboHandle );
  size_t needed_vtx_size = total_vtx_count * sizeof( ImDrawVert );
  if ( g_VboSize < needed_vtx_size )
  {
    g_VboSize = needed_vtx_size + 5000 * sizeof( ImDrawVert ); // Grow buffer
    glBufferData( GL_ARRAY_BUFFER, g_VboSize, NULL, GL_STREAM_DRAW );
  }

  unsigned char *buffer_data =
    (unsigned char *)glMapBuffer( GL_ARRAY_BUFFER, GL_WRITE_ONLY );
  if ( !buffer_data )
    return;
  for ( int n = 0; n < cmd_lists_count; n++ )
  {
    const ImDrawList *cmd_list = cmd_lists[n];
    memcpy( buffer_data, &cmd_list->vtx_buffer[0],
            cmd_list->vtx_buffer.size() * sizeof( ImDrawVert ) );
    buffer_data += cmd_list->vtx_buffer.size() * sizeof( ImDrawVert );
  }
  glUnmapBuffer( GL_ARRAY_BUFFER );
  glBindBuffer( GL_ARRAY_BUFFER, 0 );
  glBindVertexArray( g_VaoHandle );

  int cmd_offset = 0;
  for ( int n = 0; n < cmd_lists_count; n++ )
  {
    const ImDrawList *cmd_list = cmd_lists[n];
    int vtx_offset = cmd_offset;
    const ImDrawCmd *pcmd_end = cmd_list->commands.end();
    for ( const ImDrawCmd *pcmd = cmd_list->commands.begin(); pcmd != pcmd_end;
          pcmd++ )
    {
      if ( pcmd->user_callback )
      {
        pcmd->user_callback( cmd_list, pcmd );
      }
      else
      {
        glBindTexture( GL_TEXTURE_2D, ( GLuint )( intptr_t ) pcmd->texture_id );
        glScissor( (int)pcmd->clip_rect.x, (int)( height - pcmd->clip_rect.w ),
                   (int)( pcmd->clip_rect.z - pcmd->clip_rect.x ),
                   (int)( pcmd->clip_rect.w - pcmd->clip_rect.y ) );
        glDrawArrays( GL_TRIANGLES, vtx_offset, pcmd->vtx_count );
      }
      vtx_offset += pcmd->vtx_count;
    }
    cmd_offset = vtx_offset;
  }

  glBindVertexArray( 0 );
  glDisable( GL_SCISSOR_TEST );
}

void uiOnMouseButtonInput( GLFWwindow *, int button, int action, int mods )
{
  if ( action == GLFW_PRESS && button >= 0 && button < 3 )
    g_MousePressed[button] = true;
}

void uiOnScroll( GLFWwindow *, double /*xoffset*/,
                                       double yoffset )
{
  g_MouseWheel += (float)yoffset;
}

void uiOnKeyInput( GLFWwindow *, int key, int, int action,
                                    int mods )
{
  ImGuiIO &io = ImGui::GetIO();
  if ( action == GLFW_PRESS )
    io.KeysDown[key] = true;
  if ( action == GLFW_RELEASE )
    io.KeysDown[key] = false;

  io.KeyCtrl =
    io.KeysDown[GLFW_KEY_LEFT_CONTROL] || io.KeysDown[GLFW_KEY_RIGHT_CONTROL];
  io.KeyShift =
    io.KeysDown[GLFW_KEY_LEFT_SHIFT] || io.KeysDown[GLFW_KEY_RIGHT_SHIFT];
  io.KeyAlt = io.KeysDown[GLFW_KEY_LEFT_ALT] || io.KeysDown[GLFW_KEY_RIGHT_ALT];
}

void uiOnCharInput(GLFWwindow*, unsigned int c)
{
    ImGuiIO& io = ImGui::GetIO();
    if (c > 0 && c < 0x10000)
        io.AddInputCharacter((unsigned short)c);
}

void uiCreateFontsTexture()
{
  ImGuiIO &io = ImGui::GetIO();

  unsigned char *pixels;
  int width, height;
  io.Fonts->GetTexDataAsRGBA32( &pixels, &width, &height );

  glGenTextures( 1, &g_FontTexture );
  glBindTexture( GL_TEXTURE_2D, g_FontTexture );
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
  glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA,
                GL_UNSIGNED_BYTE, pixels );

  io.Fonts->TexID = (void *)( intptr_t ) g_FontTexture;

  io.Fonts->ClearInputData();
  io.Fonts->ClearTexData();
}

bool uiCreateResources()
{
    const GLchar *vertex_shader =
        "#version 330\n"
        "uniform mat4 ProjMtx;\n"
        "in vec2 Position;\n"
        "in vec2 UV;\n"
        "in vec4 Color;\n"
        "out vec2 Frag_UV;\n"
        "out vec4 Frag_Color;\n"
        "void main()\n"
        "{\n"
        "	Frag_UV = UV;\n"
        "	Frag_Color = Color;\n"
        "	gl_Position = ProjMtx * vec4(Position.xy,0,1);\n"
        "}\n";

    const GLchar* fragment_shader =
        "#version 330\n"
        "uniform sampler2D Texture;\n"
        "in vec2 Frag_UV;\n"
        "in vec4 Frag_Color;\n"
        "out vec4 Out_Color;\n"
        "void main()\n"
        "{\n"
        "	Out_Color = Frag_Color * texture( Texture, Frag_UV.st);\n"
        "}\n";

    g_ShaderHandle = glCreateProgram();
    g_VertHandle = glCreateShader(GL_VERTEX_SHADER);
    g_FragHandle = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(g_VertHandle, 1, &vertex_shader, 0);
    glShaderSource(g_FragHandle, 1, &fragment_shader, 0);
    glCompileShader(g_VertHandle);
    glCompileShader(g_FragHandle);
    glAttachShader(g_ShaderHandle, g_VertHandle);
    glAttachShader(g_ShaderHandle, g_FragHandle);
    glLinkProgram(g_ShaderHandle);

    g_AttribLocationTex = glGetUniformLocation(g_ShaderHandle, "Texture");
    g_AttribLocationProjMtx = glGetUniformLocation(g_ShaderHandle, "ProjMtx");
    g_AttribLocationPosition = glGetAttribLocation(g_ShaderHandle, "Position");
    g_AttribLocationUV = glGetAttribLocation(g_ShaderHandle, "UV");
    g_AttribLocationColor = glGetAttribLocation(g_ShaderHandle, "Color");

    glGenBuffers(1, &g_VboHandle);

    glGenVertexArrays(1, &g_VaoHandle);
    glBindVertexArray(g_VaoHandle);
    glBindBuffer(GL_ARRAY_BUFFER, g_VboHandle);
    glEnableVertexAttribArray(g_AttribLocationPosition);
    glEnableVertexAttribArray(g_AttribLocationUV);
    glEnableVertexAttribArray(g_AttribLocationColor);

#define OFFSETOF(TYPE, ELEMENT) ((size_t)&(((TYPE *)0)->ELEMENT))
    glVertexAttribPointer( g_AttribLocationPosition, 2, GL_FLOAT, GL_FALSE,
                           sizeof( ImDrawVert ),
                           (GLvoid *)OFFSETOF( ImDrawVert, pos ) );
    glVertexAttribPointer( g_AttribLocationUV, 2, GL_FLOAT, GL_FALSE,
                           sizeof( ImDrawVert ),
                           (GLvoid *)OFFSETOF( ImDrawVert, uv ) );
    glVertexAttribPointer( g_AttribLocationColor, 4, GL_UNSIGNED_BYTE, GL_TRUE,
                           sizeof( ImDrawVert ),
                           (GLvoid *)OFFSETOF( ImDrawVert, col ) );
#undef OFFSETOF
    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    uiCreateFontsTexture();

    return true;
}

bool uiInit( GLFWwindow *window )
{
    g_Window = window;

    ImGuiIO& io = ImGui::GetIO();
    io.KeyMap[ImGuiKey_Tab] = GLFW_KEY_TAB;
    io.KeyMap[ImGuiKey_LeftArrow] = GLFW_KEY_LEFT;
    io.KeyMap[ImGuiKey_RightArrow] = GLFW_KEY_RIGHT;
    io.KeyMap[ImGuiKey_UpArrow] = GLFW_KEY_UP;
    io.KeyMap[ImGuiKey_DownArrow] = GLFW_KEY_DOWN;
    io.KeyMap[ImGuiKey_Home] = GLFW_KEY_HOME;
    io.KeyMap[ImGuiKey_End] = GLFW_KEY_END;
    io.KeyMap[ImGuiKey_Delete] = GLFW_KEY_DELETE;
    io.KeyMap[ImGuiKey_Backspace] = GLFW_KEY_BACKSPACE;
    io.KeyMap[ImGuiKey_Enter] = GLFW_KEY_ENTER;
    io.KeyMap[ImGuiKey_Escape] = GLFW_KEY_ESCAPE;
    io.KeyMap[ImGuiKey_A] = GLFW_KEY_A;
    io.KeyMap[ImGuiKey_C] = GLFW_KEY_C;
    io.KeyMap[ImGuiKey_V] = GLFW_KEY_V;
    io.KeyMap[ImGuiKey_X] = GLFW_KEY_X;
    io.KeyMap[ImGuiKey_Y] = GLFW_KEY_Y;
    io.KeyMap[ImGuiKey_Z] = GLFW_KEY_Z;

    io.RenderDrawListsFn = uiOnRender;

    return true;
}

void uiShutdown()
{
    if (g_VaoHandle) glDeleteVertexArrays(1, &g_VaoHandle);
    if (g_VboHandle) glDeleteBuffers(1, &g_VboHandle);
    g_VaoHandle = 0;
    g_VboHandle = 0;

    glDetachShader(g_ShaderHandle, g_VertHandle);
    glDeleteShader(g_VertHandle);
    g_VertHandle = 0;

    glDetachShader(g_ShaderHandle, g_FragHandle);
    glDeleteShader(g_FragHandle);
    g_FragHandle = 0;

    glDeleteProgram(g_ShaderHandle);
    g_ShaderHandle = 0;

    if (g_FontTexture)
    {
        glDeleteTextures(1, &g_FontTexture);
        ImGui::GetIO().Fonts->TexID = 0;
        g_FontTexture = 0;
    }
    ImGui::Shutdown();
}

void uiNewFrame( float mouseX, float mouseY, float dt )
{
    if (!g_FontTexture)
    {
        uiCreateResources();
    }

    ImGuiIO& io = ImGui::GetIO();

    int w, h;
    int display_w, display_h;
    glfwGetWindowSize(g_Window, &w, &h);
    glfwGetFramebufferSize(g_Window, &display_w, &display_h);
    io.DisplaySize = ImVec2((float)display_w, (float)display_h);

    io.DeltaTime = dt;
    g_Time += dt;

    io.MousePos = ImVec2( mouseX, mouseY );

    for (int i = 0; i < 3; i++)
    {
      io.MouseDown[i] =
        g_MousePressed[i] || glfwGetMouseButton( g_Window, i ) != 0;
        g_MousePressed[i] = false;
    }

    io.MouseWheel = g_MouseWheel;
    g_MouseWheel = 0.0f;

    ImGui::NewFrame();
}
