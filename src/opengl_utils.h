#pragma once

#include <cstdint>

#define GLEW_STATIC
#include "GL/glew.h"

struct Shader
{
  uint32_t program;
  int matrixLocation, colourLocation;
};

extern Shader CreateShader( const char *vertex, const char *fragment );

extern void DeleteShader( uint32_t program );

enum
{
  VERTEX_ATTRIBUTE_POSITION = 0,
  VERTEX_ATTRIBUTE_NORMAL = 1,
  VERTEX_ATTRIBUTE_TEXTURE_COORDINATE = 2,
  VERTEX_ATTRIBUTE_COLOUR = 3,
  MAX_VERTEX_ATTRIBUTES,
};

struct OpenGLStaticMesh
{
  uint32_t vao, vbo, ibo, numIndices, numVertices, primitive, indexType;
};

struct OpenGLVertexAttribute
{
  GLuint index;
  GLint numComponents;
  GLenum componentType;
  GLboolean normalized;
  GLsizei offset;
};

extern void OpenGLDeleteStaticMesh( OpenGLStaticMesh mesh );

extern OpenGLStaticMesh
OpenGLCreateStaticMesh( const void *vertices, uint32_t numVertices,
                        uint32_t *indices, uint32_t numIndices,
                        uint32_t vertexSize,
                        OpenGLVertexAttribute *vertexAttributes,
                        uint32_t numVertexAttributes, int primitive );
extern void OpenGLDrawStaticMesh( OpenGLStaticMesh mesh );

extern OpenGLStaticMesh CreateCubeMesh();
