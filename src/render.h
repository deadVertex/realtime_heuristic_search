#ifndef RENDER_H
#define RENDER_H

#define GLEW_STATIC
#include "GL/glew.h"

#include <glm/glm.hpp>

#include "simulation.h"
#include "opengl_utils.h"

struct GraphVisualization
{
  OpenGLStaticMesh connectionsMesh, nodesMesh;
};

struct GridWorldTilesMesh
{
  OpenGLStaticMesh tile;
  uint32_t offsetBuffer, stateBuffer, numTiles;
  float *states;
};

extern GraphVisualization CreateGraphVisualization( const Graph &graph,
                                                    const NodeGrid &grid );

extern OpenGLStaticMesh BuildGridWorldObstacleMesh( const GridWorld& world );

extern GridWorldTilesMesh BuildGridWorldTilesMesh( const GridWorld& world );

extern void RenderGridWorld( const glm::mat4 &viewProjection, float scale,
                             Shader shader, GridWorldTilesMesh tilesMesh );

extern void DrawGraph( const glm::mat4 &viewProjection, Shader shader,
                       const GraphVisualization &visualization,
                       const glm::vec4 &colour );

extern void UpdateGridWorldTilesMesh( const GridWorld &world,
                                      GridWorldTilesMesh *mesh );

extern OpenGLStaticMesh CreateCircleMesh( float radius, int segments,
                                          bool fill );

#endif // RENDER_H
