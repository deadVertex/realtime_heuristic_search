#include "grid_world.h"

#include <cassert>
#include <cstdio>
#include <cstring>
#include <random>
#include <list>

const char *FindFirstNewline( const char *data )
{
  for ( uint32_t i = 0; data[i] != '\0'; ++i )
  {
    if ( data[i] == '\n' )
    {
      return data + i;
    }
  }
  return nullptr;
}

bool Deserialize( GridWorld *world, const char *data, uint32_t size )
{
  char key[20];
  char type[20];
  int width, height;
  sscanf( data, "%s %s", key, type );
  if ( strcmp( "type", key ) )
  {
    // Error expected 'type' as first key in map header.
    return false;
  }
  if ( strcmp( "octile", type ) )
  {
    // Error: only octile connectivity is supported.
    return false;
  }

  data = FindFirstNewline( data );
  if ( !data )
  {
    return false;
  }
  data++;
  sscanf( data, "%s %d", key, &height );
  if ( strcmp( "height", key ) )
  {
    // Error expected 'height' as the second key in the map header.
    return false;
  }

  data = FindFirstNewline( data );
  if ( !data )
  {
    return false;
  }
  data++;
  sscanf( data, "%s %d", key, &width );
  if ( strcmp( "width", key ) )
  {
    // Error expected 'width' as the third key in the map header.
    return false;
  }
  data = FindFirstNewline( data );
  if ( !data )
  {
    return false;
  }
  data++;
  if ( width <= 0 || height <= 0 )
  {
    // Error: Invalid map dimensions.
    return false;
  }
  data = FindFirstNewline( data );
  data++;
  world->m_width = width + 1;
  world->m_height = height;
  world->m_cells = new uint32_t[world->m_width * world->m_height];
  for ( int row = 0; row < height; ++row )
  {
    for ( int col = 0; col < width; ++col )
    {
      while ( *data == '\r' )
      {
        data++;
      }
      uint32_t cellIndex = col + ( row * width );
      if ( *data == '.' )
      {
        world->m_cells[cellIndex] = 0;
      }
      else
      {
        world->m_cells[cellIndex] = CELL_STATE_BLOCKED;
      }
      data++;
    }
  }
  return true;
}

void DestroyGridWorld( GridWorld *world ) { delete[] world -> m_cells; }
