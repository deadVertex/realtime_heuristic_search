#ifndef HASHMAP_H
#define HASHMAP_H

#include <cstdint>
#include <cassert>
#include <vector>
#include <cstring>

static const uint32_t MAX_ITERATIONS = 25;

template<typename T>
struct NullHasher
{
  uint32_t operator() ( const T &value ) const
  {
    return (uint32_t)value;
  }
};

template<typename T, typename K, typename H = NullHasher<K>>
class HashMap
{
public:
  HashMap( std::size_t capacity = 1024 );

  void add( K key, const T &value );
  T* retrieve( K key );
  const T* retrieve( K key ) const;
  void clear();
  bool empty() const { return m_size == 0; }
  void remove( K key );
  uint32_t get_next_index( uint32_t idx ) const;

  std::vector<T> m_values;
private:
  std::vector<uint32_t> m_keys;
  std::size_t m_size;
  H m_hasher;
};

template<typename T, typename K, typename H>
HashMap<T,K,H>::HashMap( std::size_t capacity )
  : m_size( 0 )
{
  m_keys.resize( capacity, 0 );
  m_values.resize( capacity );
}

template<typename T, typename K, typename H>
void HashMap<T,K,H>::add( K key, const T &value )
{
  if ( m_size < m_keys.size() )
  {
    uint32_t hashedKey = m_hasher( key );
    assert( hashedKey != 0 );
    uint32_t idx = hashedKey % m_keys.size();
    int i = 0;
    int sign = 1;
    for ( uint32_t iteration = 0; iteration < MAX_ITERATIONS; ++iteration )
    {
      if ( m_keys[idx] == 0 )
      {
        m_keys[idx] = hashedKey;
        m_values[idx] = value;
        m_size++;
        return;
      }
      else if ( m_keys[idx] == hashedKey )
      {
        return;
      }
      if ( sign > 0 )
      {
        i++;
      }
      idx = ( hashedKey + sign * i * i ) % m_keys.size();
      sign *= -1;
    }
    assert( 0 );
  }
  assert( 0 );
}

template<typename T, typename K, typename H>
T* HashMap<T,K,H>::retrieve( K key )
{
  const auto *constThis = const_cast<const HashMap<T,K,H>*>( this );
  return const_cast<T*>( constThis->retrieve( key ) );
}

template<typename T, typename K, typename H>
const T* HashMap<T,K,H>::retrieve( K key ) const
{
  uint32_t hashedKey = m_hasher( key );
  assert( hashedKey != 0 );
  uint32_t idx = hashedKey % m_keys.size();
  int i = 0;
  int sign = 1;
  for ( uint32_t iteration = 0; iteration < MAX_ITERATIONS; ++iteration )
  {
    if ( m_keys[idx] == hashedKey )
    {
      return &m_values[idx];
    }
    else if ( m_keys[idx] == 0 )
    {
      return nullptr;
    }
    if ( sign > 0 )
    {
      i++;
    }
    idx = ( hashedKey + sign * i * i ) % m_keys.size();
    sign *= -1;
  }
  assert( 0 );
  return nullptr;
}

template<typename T, typename K, typename H>
void HashMap<T,K,H>::clear()
{
  m_size = 0;
  std::fill( m_keys.begin(), m_keys.end(), 0 );
}

template<typename T, typename K, typename H>
void HashMap<T,K,H>::remove( K key )
{
  uint32_t hashedKey = m_hasher( key );
  assert( hashedKey != 0 );
  uint32_t idx = hashedKey % m_keys.size();
  uint32_t overwriteIdx = 0, prevIdx = 0;
  bool found = false;
  int i = 0;
  int sign = 1;
  while ( m_keys[idx] != 0 ) // TODO: Bound search
  {
    if ( m_keys[idx] == hashedKey )
    {
      overwriteIdx = idx;
      found = true;
    }
    prevIdx = idx;
    if ( sign > 0 )
    {
      i++;
    }
    idx = ( hashedKey + sign * i * i ) % m_keys.size();
    sign *= -1;
  }
  if ( found )
  {
    if ( overwriteIdx != prevIdx )
    {
      m_keys[overwriteIdx] = m_keys[prevIdx];
      m_keys[prevIdx] = 0;
      m_values[overwriteIdx] = m_values[prevIdx];
    }
    else
    {
      m_keys[overwriteIdx] = 0;
    }
    m_size--;
  }
}

template< typename T, typename K, typename H>
uint32_t HashMap<T,K,H>::get_next_index( uint32_t index ) const
{
  for ( uint32_t i = index + 1; i < m_keys.size(); ++i )
  {
    if ( m_keys[i] != 0 )
    {
      return i;
    }
  }
  return m_keys.size();
}

template<typename K, typename H = NullHasher<K>>
class HashSet
{
public:
  HashSet( std::size_t capacity = 1024 );

  void clear();
  void remove( K key );

  void add( K key );
  bool contains( K key ) const;

private:
  std::vector<uint32_t> m_keys;
  std::size_t m_size;
  H m_hasher;
};

template<typename K, typename H>
HashSet<K,H>::HashSet( std::size_t capacity )
  : m_size( 0 )
{
  m_keys.resize( capacity, 0 );
}

template<typename K, typename H>
void HashSet<K,H>::clear()
{
  m_size = 0;
  std::fill( m_keys.begin(), m_keys.end(), 0 );
}

template<typename K, typename H>
void HashSet<K,H>::add( K key )
{
  if ( m_size < m_keys.size() )
  {
    uint32_t hashedKey = m_hasher( key );
    assert( hashedKey != 0 );
    uint32_t idx = hashedKey % m_keys.size();
    int i = 0;
    int sign = 1;
    for ( uint32_t iteration = 0; iteration < 10; ++iteration )
    {
      if ( m_keys[idx] == 0 )
      {
        m_keys[idx] = hashedKey;
        m_size++;
        return;
      }
      else if ( m_keys[idx] == hashedKey )
      {
        return;
      }
      if ( sign > 0 )
      {
        i++;
      }
      idx = ( hashedKey + sign * i * i ) % m_keys.size();
      sign *= -1;
    }
    assert( 0 );
  }
  assert( 0 );
}

template<typename K, typename H>
void HashSet<K,H>::remove( K key )
{
  uint32_t hashedKey = m_hasher( key );
  assert( hashedKey != 0 );
  uint32_t idx = hashedKey % m_keys.size();
  uint32_t overwriteIdx = 0, prevIdx = 0;
  bool found = false;
  int i = 0;
  int sign = 1;
  while ( m_keys[idx] != 0 )
  {
    if ( m_keys[idx] == hashedKey )
    {
      overwriteIdx = idx;
      found = true;
    }
    prevIdx = idx;
    if ( sign > 0 )
    {
      i++;
    }
    idx = ( hashedKey + sign * i * i ) % m_keys.size();
    sign *= -1;
  }
  if ( found )
  {
    if ( overwriteIdx != prevIdx )
    {
      m_keys[overwriteIdx] = m_keys[prevIdx];
      m_keys[prevIdx] = 0;
    }
    else
    {
      m_keys[overwriteIdx] = 0;
    }
    m_size--;
  }
}

template<typename K, typename H>
bool HashSet<K,H>::contains( K key ) const
{
  uint32_t hashedKey = m_hasher( key );
  assert( hashedKey != 0 );
  uint32_t idx = hashedKey % m_keys.size();
  int i = 0;
  int sign = 1;
  for ( uint32_t iteration = 0; m_keys[idx] != 0 && iteration < 10;
        ++iteration )
  {
    if ( m_keys[idx] == hashedKey )
    {
      return true;
    }
    if ( sign > 0 )
    {
      i++;
    }
    idx = ( hashedKey + sign * i * i ) % m_keys.size();
    sign *= -1;
  }
  return false;
}

class BinarySet
{
public:
  BinarySet( uint32_t capacity = 0 );
  void clear();
  void add( uint32_t value );
  bool contains( uint32_t value ) const;
  void remove( uint32_t value );

private:
  std::vector<uint32_t> m_data;
};

inline BinarySet::BinarySet( uint32_t capacity )
{
  m_data.resize( ( capacity / 32 ) + 1, 0 );
}

inline void BinarySet::clear()
{
  std::fill( m_data.begin(), m_data.end(), 0 );
}

inline void BinarySet::add( uint32_t value )
{
  uint32_t idx = value / 32;
  uint32_t bit = value % 32;
  assert( idx < m_data.size() );

  m_data[idx] |= 1 << bit;
}

inline bool BinarySet::contains( uint32_t value ) const
{
  uint32_t idx = value / 32;
  uint32_t bit = value % 32;
  uint32_t mask = 1 << bit;
  assert( idx < m_data.size() );

  return ( m_data[idx] & mask ) != 0;
}

inline void BinarySet::remove( uint32_t value )
{
  uint32_t idx = value / 32;
  uint32_t bit = value % 32;
  assert( idx < m_data.size() );

  m_data[idx] &= ~( 1 << bit );
}

#if 0
// TODO: Decide if this should allocate its own memory.
struct BinarySet
{
  uint32_t *data, capacity;
};

inline BinarySet BinarySetCreate( uint32_t *array, uint32_t size )
{
  BinarySet set = {};
  set.capacity = size * 32;
  set.data = array;
  memset( set.data, 0, size * sizeof( uint32_t ) );
  return set;
}

inline void BinarySetClear( BinarySet *set )
{
  memset( set->data, 0, set->capacity * 8 );
}

inline void BinarySetAdd( BinarySet *set, uint32_t value )
{
  uint32_t idx = value / 32;
  uint32_t bit = value % 32;

  set->data[idx] |= 1 << bit;
}

inline bool BinarySetContains( const BinarySet *set, uint32_t value )
{
  uint32_t idx = value / 32;
  uint32_t bit = value % 32;
  uint32_t mask = 1 << bit;

  return ( set->data[idx] & mask ) != 0;
}

inline void BinarySetRemove( BinarySet *set, uint32_t value )
{
  uint32_t idx = value / 32;
  uint32_t bit = value % 32;

  set->data[idx] &= ~( 1 << bit );
}
#endif

#endif // HASHMAP_H
