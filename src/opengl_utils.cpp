#include "opengl_utils.h"

#include <cstring>
#include <cstdio>

#include <string>

static Shader GetUniformLocations( uint32_t program )
{
  Shader result = {};
  result.program = program;
  result.colourLocation = glGetUniformLocation( result.program, "colour" );
  result.matrixLocation =
    glGetUniformLocation( result.program, "in_mCombined" );
  return result;
}

typedef void ( *ShaderErrorCallback_t )( const char* );

static bool CompileShader(GLuint shader, const char* src, bool isVertex,
                          ShaderErrorCallback_t callback )
{
    int len = strlen(src);
    glShaderSource(shader, 1, &src, &len);
    glCompileShader(shader);

    int success;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        std::string buffer;
        int maxLength = 0;
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &maxLength);
        buffer.resize( maxLength );
        glGetShaderInfoLog( shader, maxLength, &maxLength, &buffer[0] );
        if ( isVertex )
        {
          buffer = std::string( "[VERTEX]\n" ) + buffer;
        }
        else
        {
          buffer = std::string( "[FRAGMENT]\n" ) + buffer;
        }
        callback( buffer.c_str() );
        return false;
    }
    return true;
}

static bool LinkShader(GLuint vertex, GLuint fragment, GLuint program,
                       ShaderErrorCallback_t callback )
{
    glAttachShader(program, vertex);
    glAttachShader(program, fragment);
    glLinkProgram(program);

    int success;
    glGetProgramiv(program, GL_LINK_STATUS, &success);
    if (!success)
    {
        char buffer[ 2048 ];
        int len = 0;
        char *cursor = strcpy( buffer, "[LINKER]\n");
        glGetProgramInfoLog( program, cursor - buffer, &len, cursor );
        callback( buffer );
        return false;
    }
    return true;
}

void DeleteShader( GLuint program )
{
    glUseProgram( 0 );
    GLsizei count = 0;
    GLuint shaders[ 2 ];
    glGetAttachedShaders( program, 2, &count, shaders );
    glDeleteProgram( program );
    glDeleteShader( shaders[ 0 ] );
    glDeleteShader( shaders[ 1 ] );
}

void PrintError( const char *msg )
{
  printf( "Shader Error: %s\n", msg );
}

Shader CreateShader( const char *vertexSource, const char *fragmentSource )
{
  Shader result = {};
  GLuint vertex = glCreateShader( GL_VERTEX_SHADER );
  GLuint fragment = glCreateShader( GL_FRAGMENT_SHADER );
  result.program = glCreateProgram();

  bool vertexSuccess = CompileShader( vertex, vertexSource, true, PrintError );
  bool fragmentSuccess = CompileShader( fragment, fragmentSource, false,
      PrintError );

  if ( vertexSuccess && fragmentSuccess )
  {
    if ( LinkShader( vertex, fragment, result.program, PrintError ) )
    {
      result = GetUniformLocations( result.program );
      return result;
    }
  }
  DeleteShader( result.program );
  result.program = 0;
  return result;
}

void OpenGLDeleteStaticMesh( OpenGLStaticMesh mesh )
{
  glBindVertexArray( 0 );
  glDeleteVertexArrays( 1, &mesh.vao );
  glDeleteBuffers( 1, &mesh.vbo );
  if ( mesh.numIndices )
  {
    glDeleteBuffers( 1, &mesh.ibo );
  }
}

inline const void* ConvertUint32ToPointer( uint32_t i )
{
  return (const void*)( (uint8_t*)0 + i );
}

OpenGLStaticMesh
OpenGLCreateStaticMesh( const void *vertices, uint32_t numVertices,
                        uint32_t *indices, uint32_t numIndices,
                        uint32_t vertexSize,
                        OpenGLVertexAttribute *vertexAttributes,
                        uint32_t numVertexAttributes, int primitive )
{
  OpenGLStaticMesh mesh = {};
  mesh.primitive = primitive;
  mesh.indexType =
    GL_UNSIGNED_INT; // TODO: Use better index type if numVertices allowes it.
  mesh.numIndices = numIndices;
  mesh.numVertices = numVertices;

  glGenVertexArrays( 1, &mesh.vao );
  glBindVertexArray( mesh.vao );

  glGenBuffers( 1, &mesh.vbo );
  if ( mesh.numIndices )
  {
    glGenBuffers( 1, &mesh.ibo );

    glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, mesh.ibo );
    glBufferData( GL_ELEMENT_ARRAY_BUFFER, sizeof( uint32_t ) * numIndices,
        indices, GL_STATIC_DRAW );
  }

  glBindBuffer( GL_ARRAY_BUFFER, mesh.vbo );

  glBufferData( GL_ARRAY_BUFFER, vertexSize * numVertices, vertices,
                GL_STATIC_DRAW );

  for ( uint32_t i = 0; i < numVertexAttributes; ++i )
  {
    OpenGLVertexAttribute *attrib = vertexAttributes + i;
    if ( attrib->index >= MAX_VERTEX_ATTRIBUTES )
    {
      OpenGLDeleteStaticMesh( mesh );
      // TODO: Zero memory.
      mesh.numIndices = 0;
      break;
    }
    glEnableVertexAttribArray( attrib->index);

    glVertexAttribPointer( attrib->index, attrib->numComponents,
                           attrib->componentType, attrib->normalized,
                           vertexSize,
                           ConvertUint32ToPointer( attrib->offset ) );
  }

  glBindVertexArray( 0 );
  return mesh;
}

void OpenGLDrawStaticMesh( OpenGLStaticMesh mesh )
{
  glBindVertexArray( mesh.vao );
  if ( mesh.numIndices )
  {
    glDrawElements( mesh.primitive, mesh.numIndices, mesh.indexType, NULL );
  }
  else
  {
    glDrawArrays( mesh.primitive, 0, mesh.numVertices );
  }
}

OpenGLStaticMesh CreateCubeMesh()
{
    float vertices[] = {
      // Top
      -0.5f, 0.5f, -0.5f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f,
      0.5f, 0.5f, -0.5f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f,
      0.5f, 0.5f, 0.5f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f,
      -0.5f, 0.5f, 0.5f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f,

      // Bottom
      -0.5f, -0.5f, -0.5f, 0.0f, -1.0f, 0.0f, 0.0f, 1.0f,
      0.5f, -0.5f, -0.5f, 0.0f, -1.0f, 0.0f, 1.0f, 1.0f,
      0.5f, -0.5f, 0.5f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f,
      -0.5f, -0.5f, 0.5f, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f,

      // Back
      -0.5f, 0.5f, -0.5f, 0.0f, 0.0f, -1.0f, 1.0f, 1.0f,
      0.5f, 0.5f, -0.5f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f,
      0.5f, -0.5f, -0.5f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f,
      -0.5f, -0.5f, -0.5f, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f,

      // Front
      -0.5f, 0.5f, 0.5f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f,
      0.5f, 0.5f, 0.5f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f,
      0.5f, -0.5f, 0.5f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f,
      -0.5f, -0.5f, 0.5f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,

      // Left
      -0.5f, 0.5f, -0.5f, -1.0f, 0.0f, 0.0f, 0.0f, 1.0f,
      -0.5f, -0.5f, -0.5f, -1.0f, 0.0f, 0.0f, 1.0f, 1.0f,
      -0.5f, -0.5f, 0.5f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f,
      -0.5f, 0.5f, 0.5f, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f,

      // Right
      0.5f, 0.5f, -0.5f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f,
      0.5f, -0.5f, -0.5f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f,
      0.5f, -0.5f, 0.5f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f,
      0.5f, 0.5f, 0.5f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f,
    };

    uint32_t indices[] = {
      2, 1, 0,
      0, 3, 2,

      4, 5, 6,
      6, 7, 4,

      8, 9, 10,
      10, 11, 8,

      14, 13, 12,
      12, 15, 14,

      16, 17, 18,
      18, 19, 16,

      22, 21, 20,
      20, 23, 22
    };

    OpenGLVertexAttribute attribs[3];
    attribs[0].index = VERTEX_ATTRIBUTE_POSITION;
    attribs[0].numComponents = 3;
    attribs[0].componentType = GL_FLOAT;
    attribs[0].normalized = GL_FALSE;
    attribs[0].offset = 0;
    attribs[1].index = VERTEX_ATTRIBUTE_NORMAL;
    attribs[1].numComponents = 3;
    attribs[1].componentType = GL_FLOAT;
    attribs[1].normalized = GL_FALSE;
    attribs[1].offset = sizeof( float ) * 3;
    attribs[2].index = VERTEX_ATTRIBUTE_TEXTURE_COORDINATE;
    attribs[2].numComponents = 2;
    attribs[2].componentType = GL_FLOAT;
    attribs[2].normalized = GL_FALSE;
    attribs[2].offset = sizeof( float ) * 6;

    return OpenGLCreateStaticMesh( vertices, 24, indices, 36,
                                   sizeof( float ) * 8, attribs, 3,
                                   GL_TRIANGLES );
}
