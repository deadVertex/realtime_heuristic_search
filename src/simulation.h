#ifndef SIMULATION_H
#define SIMULATION_H

#include "grid_world.h"
#include "graph.h"
#include "astar.h"

#include <glm/vec3.hpp>

static const uint32_t MAX_PATH_FINDING_REQUESTS = 0xFF;
struct PathFindingRequest
{
  uint32_t startNode, goalNode, pathLength;
};

struct Roboto
{
  NodePosition gridP;
  glm::vec3 newWorldP, oldWorldP;
  float newAngle, oldAngle;
  uint32_t currentCell, targetCell, goalCell, lastCell, iteration;
  uint32_t currentPathFindingRequest, cellsTravelled;
};

struct Simulation
{
  GridWorld world;
  Graph graph;
  NodeGrid nodeGrid;

  PathFindingRequest pathFindingRequests[MAX_PATH_FINDING_REQUESTS];
  uint32_t numPathFindingRequests, iterationsPerRequest;

  Roboto roboto;

  AStarContext *aStarContext;

  bool pauseSimulation;
};

extern bool InitializeSimulation( Simulation *simulation, const char *config );
#endif // SIMULATION_H
