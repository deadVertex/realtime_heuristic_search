#include "render.h"

#include <cstring>

#include "glm/gtc/type_ptr.hpp"
#include <glm/gtc/matrix_transform.hpp>

inline glm::vec3 CreateCirclePoint( float segment, float radius )
{
  float angle = segment * glm::pi< float >() * 2.0f;
  glm::vec3 v( glm::cos( angle ), glm::sin( angle ), 0.0f );
  return ( v * radius );
}

struct BuildCircleResult
{
    uint32_t numVertices, numIndices;
};

// TODO: Array bounds checking
static BuildCircleResult BuildCircle( glm::vec3 *vertices, uint32_t *indices,
                                      uint32_t vertexOffset,
                                      glm::vec3 center, float radius,
                                      int segments, bool fill )
{
    uint32_t numVertices = vertexOffset;
    uint32_t numIndices = 0;

    if ( fill )
    {
        vertices[ numVertices++ ] = center;
    }
    for ( int segment = 0; segment < segments; ++segment )
    {
        indices[ numIndices++ ] = numVertices;
        float t = (float)segment / segments;
        vertices[ numVertices++ ] = center + CreateCirclePoint( t, radius );
        if ( fill )
        {
            indices[ numIndices++ ] = vertexOffset;
        }
        indices[ numIndices++ ] = numVertices;
    }
    if ( fill )
    {
        indices[ numIndices - 1 ] = vertexOffset + 1;
    }
    else
    {
      indices[ numIndices - 1 ] = vertexOffset;
    }

    BuildCircleResult result;
    result.numVertices = numVertices - vertexOffset;
    result.numIndices = numIndices;
    return result;
}

OpenGLStaticMesh CreateCircleMesh( float radius, int segments, bool fill )
{
    glm::vec3 vertices[ 2048 ];
    uint32_t indices[ 2048 ];

    auto result = BuildCircle( vertices, indices, 0, glm::vec3(), radius,
                               segments, fill );

    OpenGLVertexAttribute attributes[1];
    attributes[0].index = VERTEX_ATTRIBUTE_POSITION;
    attributes[0].numComponents = 3;
    attributes[0].componentType = GL_FLOAT;
    attributes[0].normalized = false;
    attributes[0].offset = 0;
    return OpenGLCreateStaticMesh(
      vertices, result.numVertices, indices, result.numIndices,
      sizeof( glm::vec3 ), attributes, 1, fill ? GL_TRIANGLES : GL_LINES );
}
#if 0

void DrawPath( uint32_t *path, uint32_t pathLength,
               const NodeGrid &grid, const glm::mat4 &viewProjection,
               const glm::vec4 &colour, GLBufferedMesh pathMesh,
               GLint matrixLocation, GLint colourLocation )
{
    if ( pathLength == 0 || pathLength > 4096 )
    {
      return;
    }
    uint32_t numVertices = 0;
    glm::vec3 vertices[ 2 * 4096 ];
    for ( uint32_t i = 0; i < pathLength - 1; ++i )
    {
        auto result1 = grid.GetNodePosition( path[ i ] );
        assert( result1.isValid );
        auto result2 = grid.GetNodePosition( path[ i + 1 ] );
        assert( result2.isValid );

        auto p1 = result1.position;
        auto p2 = result2.position;

        glm::vec3 *v1 = vertices + numVertices++;
        v1->x = p1.x + 0.5f;
        v1->y = p1.y + 0.5f;
        glm::vec3 *v2 = vertices + numVertices++;
        v2->x = p2.x + 0.5f;
        v2->y = p2.y + 0.5f;
    }
    glBindVertexArray( 0 );
    glBindBuffer( GL_ARRAY_BUFFER, pathMesh.vertexBuffer );
    GLsizei size = numVertices * sizeof( float ) * 3;
    glEnableVertexAttribArray( VERTEX_POSITION );
    glVertexAttribPointer( VERTEX_POSITION, 3, GL_FLOAT, GL_FALSE, 0,
      nullptr );

    glBufferSubData( GL_ARRAY_BUFFER, 0, size, vertices );
		glUniformMatrix4fv( matrixLocation, 1, GL_FALSE,
		                    glm::value_ptr( viewProjection ) );
		glUniform4fv( colourLocation, 1, glm::value_ptr( colour ) );
    glDrawArrays( GL_LINES, 0, numVertices );
}
#endif

static OpenGLStaticMesh
CreateGraphConnectionsVisualization( const Graph &graph, const NodeGrid &grid )
{
  glm::vec3 *vertices = new glm::vec3[grid.GetNumNodes()];
  uint32_t *indices = new uint32_t[graph.GetNumConnections() * 2];

  uint32_t numIndices = 0;

  for ( uint32_t node = 0; node < grid.GetNumNodes(); ++node )
  {
    auto result = grid.GetNodePosition( node );
    if ( result.isValid )
    {
      auto p = result.position;
      vertices[node] = glm::vec3( p.x + 0.5f, p.y + 0.5f, 0.0f );

      Graph::Connection connections[8];
      auto numConnections = graph.GetConnections( connections, 8, node );
      for ( uint32_t connectionIdx = 0; connectionIdx < numConnections;
            ++connectionIdx )
      {
        uint32_t otherNode = connections[connectionIdx].m_to;
        indices[numIndices++] = node;
        indices[numIndices++] = otherNode;
      }
    }
  }

  OpenGLVertexAttribute attributes[1];
  attributes[0].index = VERTEX_ATTRIBUTE_POSITION;
  attributes[0].numComponents = 3;
  attributes[0].componentType = GL_FLOAT;
  attributes[0].normalized = false;
  attributes[0].offset = 0;
  auto mesh =
    OpenGLCreateStaticMesh( vertices, grid.GetNumNodes(), indices, numIndices,
                            sizeof( glm::vec3 ), attributes, 1, GL_LINES );
  delete[] vertices;
  delete[] indices;
  return mesh;
}

static OpenGLStaticMesh CreateGraphNodesVisualization( const NodeGrid &grid )
{
  OpenGLStaticMesh result = {};
  glm::vec3 *vertices = new glm::vec3[grid.GetNumNodes() * 9];
  uint32_t *indices = new uint32_t[grid.GetNumNodes() * 24];

  uint32_t numVertices = 0;
  uint32_t numIndices = 0;

  for ( uint32_t node = 0; node < grid.GetNumNodes(); ++node )
  {
    auto result = grid.GetNodePosition( node );
    if ( result.isValid )
    {
      auto p = result.position;
      auto circle =
        BuildCircle( vertices, indices + numIndices, numVertices,
                     glm::vec3( p.x + 0.5f, p.y + 0.5f, 0.0f ), 0.1f, 8, true );
      numVertices += circle.numVertices;
      numIndices += circle.numIndices;
    }
  }

  OpenGLVertexAttribute attributes[1];
  attributes[0].index = VERTEX_ATTRIBUTE_POSITION;
  attributes[0].numComponents = 3;
  attributes[0].componentType = GL_FLOAT;
  attributes[0].normalized = false;
  attributes[0].offset = 0;
  result =
    OpenGLCreateStaticMesh( vertices, numVertices, indices, numIndices,
                            sizeof( glm::vec3 ), attributes, 1, GL_TRIANGLES );
  delete[] vertices;
  delete[] indices;
  return result;
}

GraphVisualization CreateGraphVisualization( const Graph &graph,
                                                    const NodeGrid &grid )
{
  GraphVisualization result = {};
  result.connectionsMesh = CreateGraphConnectionsVisualization( graph, grid );
  result.nodesMesh = CreateGraphNodesVisualization( grid );
  return result;
}

OpenGLStaticMesh BuildGridWorldObstacleMesh( const GridWorld& world )
{
  OpenGLStaticMesh result;
  uint32_t maxVertices = world.m_width * world.m_height * 8;
  uint32_t maxIndices = world.m_width * world.m_height * 36;
  glm::vec3* vertices = new glm::vec3[maxVertices];
  uint32_t* indices = new uint32_t[maxIndices];

  assert( vertices );
  uint32_t numVertices = 0;
  uint32_t numIndices = 0;
  for ( uint32_t y = 0; y < world.m_height; ++y )
  {
    for ( uint32_t x = 0; x < world.m_width; ++x )
    {
      uint32_t cellIndex = x + ( y * world.m_width );
      if ( ( world.m_cells[cellIndex] & CELL_STATE_BLOCKED ) ||
           ( x == world.m_width - 1 ) || ( y == world.m_height - 1 ) )
      {
        uint32_t localIndices[] = {0, 2, 1, 0, 3, 2,

                                   1, 2, 6, 6, 5, 1,

                                   4, 5, 6, 6, 7, 4,

                                   2, 3, 6, 6, 3, 7,

                                   0, 7, 3, 0, 4, 7,

                                   0, 1, 5, 0, 5, 4};
        for ( uint32_t i = 0; i < 36; ++i )
        {
          localIndices[i] += numVertices;
        }
        assert( ( numIndices + 36 ) < maxIndices );
        std::copy_n( localIndices, 36, indices + numIndices );
        numIndices += 36;

        glm::vec3 positions[] = {
          glm::vec3{-0.5, -0.5, 0.5},  glm::vec3{0.5, -0.5, 0.5},
          glm::vec3{0.5, 0.5, 0.5},    glm::vec3{-0.5, 0.5, 0.5},

          glm::vec3{-0.5, -0.5, -0.5}, glm::vec3{0.5, -0.5, -0.5},
          glm::vec3{0.5, 0.5, -0.5},   glm::vec3{-0.5, 0.5, -0.5}};

        assert( ( numVertices + 8 ) < maxVertices );
        for ( uint32_t i = 0; i < 8; ++i )
        {
          vertices[numVertices++] =
            positions[i] + glm::vec3( x + 0.5f, y + 0.5f, 0 );
        }
      }
    }
  }
  OpenGLVertexAttribute attributes[1];
  attributes[0].index = VERTEX_ATTRIBUTE_POSITION;
  attributes[0].numComponents = 3;
  attributes[0].componentType = GL_FLOAT;
  attributes[0].normalized = false;
  attributes[0].offset = 0;
  result = OpenGLCreateStaticMesh( vertices, numVertices, indices, numIndices,
      sizeof( glm::vec3 ), attributes, 1, GL_TRIANGLES );
  delete[] vertices;
  delete[] indices;
  return result;
}

static const uint32_t VERTEX_ATTRIBUTE_OFFSET = 8;
static const uint32_t VERTEX_ATTRIBUTE_STATE = 9;
GridWorldTilesMesh BuildGridWorldTilesMesh( const GridWorld& world )
{
  GridWorldTilesMesh result = {};
  result.numTiles = world.m_width * world.m_height;
  //uint32_t maxVertices = ( world.m_width * 2 ) + ( world.m_height * 2 );
  //glm::vec3* vertices = new glm::vec3[maxVertices];
  //uint32_t numVertices = 0;
  //for ( uint32_t x = 0; x < world.m_width; ++x )
  //{
    //vertices[numVertices++] = glm::vec3( x, 0, 0 );
    //vertices[numVertices++] = glm::vec3( x, world.m_height, 0 );
  //}
  //for ( uint32_t y = 0; y < world.m_height; ++y )
  //{
    //vertices[numVertices++] = glm::vec3( 0, y, 0 );
    //vertices[numVertices++] = glm::vec3( world.m_width, y, 0 );
  //}
  //GLBufferedMesh mesh = CreateBufferedMesh( vertices, numVertices );
  //delete[] vertices;
  //return mesh;

  glm::vec3 vertices[4];
  float s = 0.48f;
  vertices[0] = glm::vec3{ -s, s, 0.0f };
  vertices[1] = glm::vec3{ -s, -s, 0.0f };
  vertices[2] = glm::vec3{ s, -s, 0.0f };
  vertices[3] = glm::vec3{ s, s, 0.0f };

  uint32_t indices[6] = { 0, 1, 2, 2, 3, 0 };

  OpenGLVertexAttribute attributes[1];
  attributes[0].index = VERTEX_ATTRIBUTE_POSITION;
  attributes[0].numComponents = 3;
  attributes[0].componentType = GL_FLOAT;
  attributes[0].normalized = false;
  attributes[0].offset = 0;
  result.tile = OpenGLCreateStaticMesh(
    vertices, 4, indices, 6, sizeof( glm::vec3 ), attributes, 1, GL_TRIANGLES );

  std::vector<glm::vec2> offsets;
  offsets.reserve( world.m_width * world.m_height );
  for ( uint32_t iy = 0; iy < world.m_height; ++iy )
  {
    for ( uint32_t ix = 0; ix < world.m_width; ++ix )
    {
      offsets.push_back( glm::vec2{ ix + 0.5f, iy + 0.5f } );
    }
  }

  glBindVertexArray( result.tile.vao );
  glGenBuffers( 1, &result.offsetBuffer );
  glBindBuffer( GL_ARRAY_BUFFER, result.offsetBuffer );
  glBufferData( GL_ARRAY_BUFFER, sizeof( glm::vec2 ) * offsets.size(),
                offsets.data(), GL_STATIC_DRAW );
  glEnableVertexAttribArray( VERTEX_ATTRIBUTE_OFFSET );
  glVertexAttribPointer( VERTEX_ATTRIBUTE_OFFSET, 2, GL_FLOAT, GL_FALSE,
                         sizeof( glm::vec2 ), 0 );
  glVertexAttribDivisor( VERTEX_ATTRIBUTE_OFFSET, 1 );

  result.states = new float[world.m_width * world.m_height];

  // TODO: Remove duplication
  for ( uint32_t i = 0; i < world.m_width * world.m_height; ++i )
  {
    if ( world.m_cells[i] & CELL_STATE_EXPLORED )
    {
      if ( world.m_cells[i] & CELL_STATE_BLOCKED )
      {
        result.states[i] = 0.0f;
      }
      else
      {
        result.states[i] = 0.8f;
      }
    }
    else
    {
      result.states[i] = 0.15;
    }
  }

  glGenBuffers( 1, &result.stateBuffer );
  glBindBuffer( GL_ARRAY_BUFFER, result.stateBuffer );
  glBufferData( GL_ARRAY_BUFFER,
                sizeof( float ) * world.m_width * world.m_height, result.states,
                GL_DYNAMIC_DRAW );

  glEnableVertexAttribArray( VERTEX_ATTRIBUTE_STATE );
  glVertexAttribPointer( VERTEX_ATTRIBUTE_STATE, 1, GL_FLOAT, GL_FALSE,
                         sizeof( float ), 0 );
  glVertexAttribDivisor( VERTEX_ATTRIBUTE_STATE, 1 );

  glBindVertexArray( 0 );
  return result;
}

void RenderGridWorld( const glm::mat4 &viewProjection, float scale,
                      Shader shader, GridWorldTilesMesh gridWorldTilesMesh )
{
  glUniformMatrix4fv( shader.matrixLocation, 1, GL_FALSE,
                      glm::value_ptr( viewProjection ) );

  glBindVertexArray( gridWorldTilesMesh.tile.vao );
  glDrawElementsInstanced( GL_TRIANGLES, gridWorldTilesMesh.tile.numIndices,
                           gridWorldTilesMesh.tile.indexType, NULL,
                           gridWorldTilesMesh.numTiles );
}

void DrawGraph( const glm::mat4 &viewProjection, Shader shader,
                const GraphVisualization &visualization,
                const glm::vec4 &colour )
{
  glUniform4fv( shader.colourLocation, 1, glm::value_ptr( colour ) );
  glUniformMatrix4fv( shader.matrixLocation, 1, GL_FALSE,
                      glm::value_ptr( viewProjection ) );
  OpenGLDrawStaticMesh( visualization.nodesMesh );
  OpenGLDrawStaticMesh( visualization.connectionsMesh );
}

void UpdateGridWorldTilesMesh( const GridWorld &world,
                               GridWorldTilesMesh *mesh )
{
  uint32_t n = world.m_width * world.m_height;
  for ( uint32_t i = 0; i < n; ++i )
  {
    if ( world.m_cells[i] & CELL_STATE_EXPLORED )
    {
      if ( world.m_cells[i] & CELL_STATE_BLOCKED )
      {
        mesh->states[i] = 0.0f;
      }
      else
      {
        mesh->states[i] = 0.8f;
      }
    }
    else
    {
      mesh->states[i] = 0.15;
    }
  }
  glBindBuffer( GL_ARRAY_BUFFER, mesh->stateBuffer );
  glBufferData( GL_ARRAY_BUFFER, sizeof( float ) * n, mesh->states,
                GL_DYNAMIC_DRAW );
}
