#pragma once

#include <cassert>
#include <vector>

// Based heavily on this example code.
// http://www.cprogramming.com/tutorial/computersciencetheory/heapcode.html

template< typename T, typename Compare >
class Heap
{
public:
  Heap( std::size_t capacity );

  void Push( const T &val );
  void Pop();
  T Front();

  unsigned int GetSize() const { return m_current; }
  unsigned int GetCapacity() const { return m_array.size(); }
  bool IsEmpty() const { return GetSize() == 0; }

  void Clear() { m_current = 0; }

  bool Replace( const T &val, T *replaced = NULL );
  void ReplaceMin( const T &item );

  bool Contains( const T &val ) const;

  bool Test( const T &a, const T &b ) const { return m_compare( a, b ); }

private:
  static uint32_t ParentOf( uint32_t node ) { return ( node - 1 ) / 2; }
  static uint32_t LeftChildOf( uint32_t node ) { return ( node * 2 ) + 1; }

  void ShiftUp( uint32_t node );
  void ShiftDown( uint32_t node );

private:
  std::vector<T> m_array;
  uint32_t m_current;
  Compare m_compare;
};


template< typename T, typename Compare >
Heap< T, Compare >::Heap( std::size_t capacity )
: m_current( 0 )
{
  m_array.resize( capacity );
}

template< typename T, typename Compare >
void Heap< T, Compare >::Push( const T &val )
{
  assert( m_current < m_array.size() );
  m_array[ m_current ] = val;
  ShiftUp( m_current++ );
}

template< typename T, typename Compare >
T Heap< T, Compare >::Front()
{
  return m_array[0];
}

template< typename T, typename Compare >
void Heap< T, Compare >::Pop()
{
  m_current--;
  m_array[ 0 ] = m_array[ m_current ];
  ShiftDown( 0 );
}

template< typename T, typename Compare >
void Heap< T, Compare >::ReplaceMin( const T &item )
{
  bool found = false;
  uint32_t minIdx = 0;
  for ( uint32_t i = 1; i < m_current; ++i )
  {
    if ( m_compare( m_array[ minIdx ], m_array[ i ] ) )
    {
      minIdx = i;
      found = true;
    }
  }
  if ( found )
  {
    m_array[ minIdx ] = item;
    ShiftUp( minIdx );
  }
}

template< typename T, typename Compare >
bool Heap< T, Compare >::Replace( const T &val, T *replaced )
{
  bool result = false;
  for ( uint32_t i = 0; i < m_current; ++i )
  {
    if ( m_array[ i ] == val )
    {
      if ( m_compare( m_array[ i ], val ) )
      {
        --m_current;
        if ( replaced )
        {
          *replaced = m_array[i];
        }
        m_array[ i ] = m_array[ m_current ];
        result = true;
        ShiftDown( i );
        break;
      }
    }
  }
  Push( val );
  return result;
}

template< typename T, typename Compare >
bool Heap< T, Compare >::Contains( const T &val ) const
{
  bool result = false;
  for ( uint32_t i = 0; i < m_current; ++i )
  {
    if ( m_array[ i ] == val )
    {
      result = true;
    }
  }
  return result;
}

template< typename T, typename Compare >
void Heap< T, Compare >::ShiftUp( uint32_t node )
{
  uint32_t current = node;
  T element = m_array[ current ];

  while ( current > 0 )
  {
    int parent = ParentOf( current );
    if ( m_compare( m_array[ parent ], element ) )
    {
      m_array[ current ] = m_array[ parent ];
      current = parent;
    }
    else
      break;
  }
  m_array[ current ] = element;
}

template< typename T, typename Compare >
void Heap< T, Compare >::ShiftDown( uint32_t node )
{
  uint32_t current = node;
  uint32_t child = LeftChildOf( current );
  T el = m_array[ current ];
  while ( child < m_current )
  {
    if ( child < ( m_current - 1 ) )
    {
      if ( m_compare( m_array[ child ], m_array[ child + 1 ] ) )
        child++;
    }

    if ( m_compare( el, m_array[ child ] ) )
    {
      m_array[ current ] = m_array[ child ];
      current = child;
      child = LeftChildOf( current );
    }
    else
      break;
  }
  m_array[ current ] = el;
}
