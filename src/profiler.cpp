#include "profiler.h"

#include <GLFW/glfw3.h>
#include <cstdio>

namespace profiler
{
Profile profiles[MAX_PROFILES];
uint32_t numProfiles = 0;
}

void StartProfile( const char *name )
{
  using namespace profiler;
  if ( numProfiles < MAX_PROFILES )
  {
    auto p = profiles + numProfiles++;
    p->name = name;
    p->start = glfwGetTime();
  }
}

void EndProfile()
{
  using namespace profiler;
  if ( numProfiles > 0 )
  {
    auto p = profiles + --numProfiles;
    printf( "%s: %g seconds\n", p->name, glfwGetTime() - p->start );
  }
}
