#include "astar.h"

#include "heap.h"

struct AStarNode
{
  uint32_t node, pathLength;
  //std::vector< uint32_t > path;
  float hcost, fcost, gcost;
  int pathHead;

  bool operator <( const AStarNode &n ) const
  {
    return fcost < n.fcost;
  }

  bool operator ==( const AStarNode &n ) const
  {
    return node == n.node;
  }
};

struct CompareAStarNodes
{
  bool operator()( const AStarNode &a, const AStarNode &b ) const
  {
    return a.fcost > b.fcost;
  }
};

#define EXPANDED_LIST_CAPACITY 64091
static const uint32_t MAX_PARTIAL_PATH_SEGMENTS = MAX_NODES * 100;

struct PartialPathSegment
{
  uint32_t node;
  int next;
};

struct AStarContext
{
  Heap<AStarNode, CompareAStarNodes> queue;
  BinarySet expandedList;
  PartialPathSegment segments[MAX_PARTIAL_PATH_SEGMENTS];
  int freeListHead;
  uint32_t highestAllocatedSegment;

  AStarContext()
    : queue( 0xFFFF )
  {
  }
};

AStarContext* CreateAStarContext()
{
  auto context = new AStarContext;
  context->expandedList = BinarySet( MAX_NODES );
  for ( uint32_t i = 0; i < MAX_PARTIAL_PATH_SEGMENTS; ++i )
  {
    context->segments[i].next = i + 1;
  }
  context->segments[MAX_PARTIAL_PATH_SEGMENTS - 1].next = -1;
  return context;
}

void DestroyAStarContext( AStarContext *context )
{
  delete context;
}

inline PartialPathSegment* AllocatePartialPathSegment( AStarContext *context )
{
  PartialPathSegment *result = NULL;
  if ( context->freeListHead >= 0 )
  {
    result = context->segments + context->freeListHead;
    if ( context->freeListHead > (int)context->highestAllocatedSegment )
    {
      context->highestAllocatedSegment = context->freeListHead;
    }
    context->freeListHead = result->next;
    result->node = 0xFFFFFFFF;
    result->next = -1;
  }
  return result;
}

static int CopyAndAppendPartialPath( AStarContext *context, int src,
                                     uint32_t nodeToAppend )
{
  int result = -1;
  PartialPathSegment *srcPtr = NULL;
  PartialPathSegment *dstPtr = NULL;
  while ( src >= 0 )
  {
    srcPtr = context->segments + src;
    assert( srcPtr->node != 0xFFFFFFFF );
    auto temp = AllocatePartialPathSegment( context );
    assert( temp );
    temp->node = srcPtr->node;
    if ( dstPtr )
    {
      dstPtr->next = temp - context->segments;
    }
    else
    {
      result = temp - context->segments;
    }
    dstPtr = temp;
    src = srcPtr->next;
  }

  auto appended = AllocatePartialPathSegment( context );
  assert( appended );
  appended->node = nodeToAppend;
  if ( dstPtr )
  {
    dstPtr->next = appended - context->segments;
  }
  else
  {
    result = appended - context->segments;
  }
  return result;
}

void FreePartialPathSegments( AStarContext *context, int head )
{
  while ( head >= 0 )
  {
    auto segment = context->segments + head;
    auto temp = segment->next;
    segment->next = context->freeListHead;
    assert( segment->node != 0xFFFFFFFF );
    segment->node = 0xFFFFFFFF;
    context->freeListHead = head;
    head = temp;
  }
}

static AStarNode CreateAStarNode( uint32_t nodeId, NodePosition goalPosition,
                                  const NodeGrid &grid, AStarContext *context,
                                  const AStarNode *parent,
                                  float connectionCost )
{
  auto nodeResult = grid.GetNodePosition( nodeId );
  assert( nodeResult.isValid );
  AStarNode node;
  node.node = nodeId;
  node.hcost =
    CalculateEuclideanDistance( nodeResult.position, goalPosition );
  node.gcost = connectionCost;
  if ( parent )
  {
    node.pathHead =
      CopyAndAppendPartialPath( context, parent->pathHead, parent->node );
    node.pathLength = parent->pathLength + 1;
    node.gcost += parent->gcost;
  }
  else
  {
    node.pathHead = -1;
    node.pathLength = 0;
  }
  node.fcost = node.gcost + node.hcost;
  return node;
}

uint64_t numStatesExpanded = 0;
uint64_t maxQueueSize = 0;

int AStar( const Graph &graph, const NodeGrid &grid, uint32_t *pathNodes,
           uint32_t maxPathSize, uint32_t startNode, uint32_t goalNode,
           AStarContext *context, const SearchSpaceSet *searchSpace )
{
  auto &queue = context->queue;
  auto &expandedList = context->expandedList;

  queue.Clear();
  expandedList.clear();

  for ( uint32_t i = 0; i < context->highestAllocatedSegment + 1; ++i )
  {
    context->segments[i].next = i + 1;
  }
  context->freeListHead = 0;
  context->highestAllocatedSegment = 0;

  auto goalResult = grid.GetNodePosition( goalNode );
  assert( goalResult.isValid );
  auto goalPosition = goalResult.position;

  queue.Push(
    CreateAStarNode( startNode, goalPosition, grid, context, nullptr, 0.0f ) );

  while ( !queue.IsEmpty() )
  {
    maxQueueSize = std::max( maxQueueSize, (uint64_t)queue.GetSize() );

    auto node = queue.Front();
    queue.Pop();

    if ( node.node == goalNode )
    {
      int result = -1;
      if ( node.pathLength + 1 < maxPathSize )
      {
        int next = node.pathHead;
        for ( uint32_t i = 0; i < node.pathLength; ++i )
        {
          assert( next >= 0 );
          PartialPathSegment *srcPtr = context->segments + next;
          pathNodes[i] = srcPtr->node;
          next = srcPtr->next;
        }
        pathNodes[node.pathLength] = node.node;
        result = node.pathLength + 1;
      }
      return result;
    }

    if ( !expandedList.contains( node.node ) )
    {
      expandedList.add( node.node );

      Graph::Connection connections[MAX_CONNECTIONS_PER_NODE];
      uint32_t numConnections = graph.GetConnections(
        connections, MAX_CONNECTIONS_PER_NODE, node.node );
      for ( uint32_t i = 0; i < numConnections; ++i )
      {
        if ( !searchSpace || searchSpace->contains( connections[i].m_to ) )
        {
          auto child = CreateAStarNode( connections[i].m_to, goalPosition, grid,
                                        context, &node, connections[i].m_cost );
          if ( !expandedList.contains( child.node ) )
          {
            numStatesExpanded++;
            AStarNode replaced;
            if ( queue.Replace( child, &replaced ) )
            {
              FreePartialPathSegments( context, replaced.pathHead );
            }
            continue;
          }
          FreePartialPathSegments( context, child.pathHead );
        }
      }
    }
    FreePartialPathSegments( context, node.pathHead );
  }
  return 0;
}
