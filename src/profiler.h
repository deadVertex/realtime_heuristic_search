#pragma once

#include <cstdint>

namespace profiler
{
struct Profile
{
  const char *name;
  double start;
};

static const uint32_t MAX_PROFILES = 0xFF;

extern Profile profiles[MAX_PROFILES];
extern uint32_t numProfiles;
}

extern void StartProfile( const char *name );
extern void EndProfile();

struct AutoProfile
{
  AutoProfile( const char *name )
  {
    StartProfile( name );
  }

  ~AutoProfile()
  {
    EndProfile();
  }
};

#define PROFILE_SCOPE( NAME ) AutoProfile p( NAME )
