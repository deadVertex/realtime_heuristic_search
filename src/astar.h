#pragma once

#include <cstdint>

#include "hashmap.h"
#include "graph.h"

struct AStarContext;

extern AStarContext* CreateAStarContext();
extern void DestroyAStarContext( AStarContext *context );

typedef HashSet<uint32_t, Graph::NodeIdHasher> SearchSpaceSet;
extern int AStar( const Graph &graph, const NodeGrid &grid, uint32_t *pathNodes,
                  uint32_t maxPathSize, uint32_t startNode, uint32_t goalNode,
                  AStarContext *context,
                  const SearchSpaceSet *searchSpace = NULL );
