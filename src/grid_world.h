#ifndef GRID_WORLD_H
#define GRID_WORLD_H

#include <cstdint>

enum
{
  CELL_STATE_BLOCKED = 1,
  CELL_STATE_EXPLORED = 2,
};

struct GridWorld
{
    uint32_t m_width, m_height;
    uint32_t *m_cells;
};

extern bool Deserialize( GridWorld *world, const char *data, uint32_t size );

extern void CreateGridWorld( GridWorld *world, uint32_t width,
                             uint32_t height );

extern void DestroyGridWorld( GridWorld *world );

#endif // GRID_WORLD_H
