#ifndef __GRAPH_H__
#define __GRAPH_H__

#include <vector>
#include <unordered_map>
#include <stdint.h>
#include <cassert>
#include <algorithm>

#include "hashmap.h"

// Remove MAX_NODES as a hard limit and use it rather as a default value.
static const uint32_t MAX_NODES = 500000;
static const uint32_t MAX_CONNECTIONS_PER_NODE = 16;
static const uint32_t MAX_WIDTH = 8192;
static const uint32_t MAX_HEIGHT = 8192;
static const float DIAGONAL_COST = 1.414f;


//Hash function from https://gist.github.com/badboy/6267743
inline uint32_t HashShiftMult32(uint32_t key)
{
  uint32_t c2 = 0x27d4eb2d; // a prime or an odd constant
  key = ( key ^ 61 ) ^ ( key >> 16 );
  key = key + ( key << 3 );
  key = key ^ ( key >> 4 );
  key = key * c2;
  key = key ^ ( key >> 15 );
  return key;
}

class Graph
{
public:
  struct Connection
  {
    float m_cost;
    uint32_t m_to, m_from;
  };
  struct ConnectionArray
  {
    uint32_t numConnections;
    Connection connections[MAX_CONNECTIONS_PER_NODE];

    ConnectionArray() : numConnections( 0 ) {}
  };

  struct NodeIdHasher
  {
    uint32_t operator() ( uint32_t node ) const
    {
      return HashShiftMult32( node ) + 1;
    }
  };

public:
  Graph( uint32_t capacity = MAX_NODES );

  bool AddConnection( uint32_t to, uint32_t from, float cost );
  void RemoveConnection( uint32_t to, uint32_t from );

  uint32_t GetConnections( Connection *connections, uint32_t size,
                           uint32_t fromNode ) const;

  uint32_t GetNumConnections() const;

  uint32_t GetAllConnections( Connection *connections, uint32_t size ) const;

  uint32_t GetDegree( uint32_t node ) const;

  bool IsConnected( uint32_t node1, uint32_t node2 ) const;

private:
  uint32_t m_numConnections;
  HashMap<ConnectionArray, uint32_t, NodeIdHasher> m_hashMap;
};

struct NodePosition
{
  uint32_t x, y;
};

struct NodePositionHasher
{
  uint32_t operator() ( const NodePosition &p ) const
  {
    assert( ( p.x < MAX_WIDTH ) && ( p.y < MAX_HEIGHT ) );
    return HashShiftMult32( ( ( p.y + 1 ) << 16 ) | ( p.x + 1 ) );
  }
};

struct GetNodePositionResult
{
  NodePosition position;
  bool isValid;
};

inline bool operator ==( NodePosition a, NodePosition b )
{
  return ( a.x == b.x ) && ( a.y == b.y );
}

inline float CalculateManhattanDistance( NodePosition a, NodePosition b )
{
  auto dx = labs( (int)a.x - (int)b.x );
  auto dy = labs( (int)a.y - (int)b.y );
  return std::max( dx, dy );
}

inline float CalculateDiagonalDistance( NodePosition a, NodePosition b )
{
  float dmax =
    (float)std::max( labs( (int)a.x - (int)b.x ), labs( (int)a.y - (int)b.y ) );
  float dmin =
    (float)std::min( labs( (int)a.x - (int)b.x ), labs( (int)a.y - (int)b.y ) );
  return DIAGONAL_COST * dmin + ( dmax - dmin );
}

inline float CalculateEuclideanDistance( NodePosition a, NodePosition b )
{
  float x = (int)a.x - (int)b.x;
  float y = (int)a.y - (int)b.y;
  return sqrt( x * x + y * y );
}


class NodeGrid
{
public:
  NodeGrid( uint32_t capacity = MAX_NODES );
  uint32_t AddNode( NodePosition p );
  GetNodePositionResult GetNodePosition( uint32_t node ) const;
  bool GetNodeAtPosition( NodePosition p, uint32_t *node ) const;
  bool GetNodeAtPosition( uint32_t x, uint32_t y, uint32_t *node ) const;

  void SetNodePosition( NodePosition p, uint32_t node ) const;

  void RemoveNode( uint32_t node );
  void SetNodePosition( uint32_t node, NodePosition p );

  inline uint32_t GetNumNodes() const { return m_nodePositions.size(); }
  inline uint32_t GetXMin() const { return m_xMin; }
  inline uint32_t GetYMin() const { return m_yMin; }
  inline uint32_t GetXMax() const { return m_xMax; }
  inline uint32_t GetYMax() const { return m_yMax; }

private:
  std::vector<NodePosition> m_nodePositions;
  HashMap<uint32_t, NodePosition, NodePositionHasher> m_hashMap;
  uint32_t m_xMin, m_yMin, m_xMax, m_yMax;
};

static const uint32_t NULL_NODE = 0xFFFFFFFF;
static const uint32_t CHILD_NODE_POOL_SIZE = MAX_NODES * 3;

struct AddNodeResult
{
  bool alreadyExists;
  uint32_t node;
};
inline AddNodeResult AddNodeIfItDoesntExist( NodeGrid *grid, uint32_t x,
                                             uint32_t y )
{
  AddNodeResult result = {};
  NodePosition p = { x, y };
  if ( !grid->GetNodeAtPosition( p, &result.node ) )
  {
    result.node = grid->AddNode( p );
  }
  else
  {
    result.alreadyExists = true;
  }
  return result;
}

inline bool DoesNodeExist( const NodeGrid &grid, uint32_t x, uint32_t y )
{
  uint32_t node;
  NodePosition p = { x, y };
  return grid.GetNodeAtPosition( p, &node );
}

#endif // !__GRAPH_H__
