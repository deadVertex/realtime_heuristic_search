To compile the application you need to ensure that you have CMake installed and
that it can be accessed from the command line. Also ensure that CMake can
generate Visual Studio solutions for the version of visual studio you have
installed, this will need to be at least VS2012 or later.

1. Run setup.bat which will create a 'build' directory which if CMake was 
   successful  will contain the Visual Studio solution and project file.
2. Open simulation_platform.sln in Visual Studio
3. Build the simulation platform project

Instructions on how to use the simulation platform are supplied in the report.